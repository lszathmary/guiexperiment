
 This my failed attempt at writing a lightweight Object Oriented GUI library, that handles navigation with Gamepad, besides mouse and keyboard. The reason for attempting this, was that there aren't any GUI libraries with very well integrated gamepad navigation.

 While writing this I realized that navigating with gamepad works very differently, than mouse navigation, and the ui has to be designed around the gamepad and we can't just use traditional ui components.

 While coding this I've also discovered that some other GUI library do try to solve this problem, for example: WPF does handle gamepad navigation for example, but their implementation seems pretty complicated, and I think this problem can be solved in simpler way. I've also looked at Android SDK, to see how navigation is handled on Android TV with remote control, which behaves similarly to gamepad when used for navigation, but they had a similar approach to WPF.

 Another reason for stopping is that I felt that OOP was making the code bigger and more complex than it need at to be, and it was just getting in the way.
