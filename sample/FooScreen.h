#pragma once


#include "Screen.h"

class Application;

using namespace std;

class FooScreen :
	public inheritable_shared_from_this<FooScreen>, public Screen {

public:
    FooScreen(shared_ptr<Application> application);


    
    void init();
    virtual void onBackPressed() override;

    void set_parent(shared_ptr<Screen> screen){
        this->relative_screen = screen;
    }

protected:
    shared_ptr<Application> application;
    shared_ptr<Screen> relative_screen;
};