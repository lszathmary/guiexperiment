#pragma once


#include "Screen.h"
#include "Application.h"

using namespace std;

class MainScreen :
	public inheritable_shared_from_this<MainScreen>, public Screen {

public:
    MainScreen(shared_ptr<Application> application);

    void init();
    virtual void onBackPressed() override;

    void set_parent(shared_ptr<Screen> screen){
        this->parent = screen;
    }

    void set_game_list_screen(shared_ptr<Screen> screen){
        this->game_list_screen = screen;
    }

    void set_about_screen(shared_ptr<Screen> screen){
        this->about_screen = screen;
    }

     void set_foo_screen(shared_ptr<Screen> screen){
        this->foo_screen = screen;
    }
    

    void switchToGameScreen();
    void switchToTestScreen();
    void switchToFooScreen();

protected:
    shared_ptr<Application> application;
    shared_ptr<Screen> parent;

    shared_ptr<Screen> game_list_screen;
    shared_ptr<Screen> about_screen;
    shared_ptr<Screen> foo_screen;

};