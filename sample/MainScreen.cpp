
#include "MainScreen.h"
#include "VBoxContainer.h"
#include "Label.h"
#include "Button.h"
#include "Application.h"


MainScreen::MainScreen(shared_ptr<Application> application)
:Screen(application->graphics) {
	this->application = application;

	//init();
}

void
MainScreen::init(){

        shared_ptr<VBoxContainer> linearContainter(new VBoxContainer());
		linearContainter->setName("linearContainer");
		linearContainter->setSpacing(0);
		linearContainter->setPosition(0, 0);
		linearContainter->setSize(1280, 800);
			
		// Populate left container
		shared_ptr<Container> leftContainer(new Container());
		leftContainer->setName("leftContainer");
		leftContainer->setWidth(400);
		
		shared_ptr<Label> label(new Label());
		label->setText("This is label with some text...");
		label->setPosition(50, 550);
		label->setSize(300, 100);
		leftContainer->addChild(label);
				
		shared_ptr<Button> button1 = make_shared<Button>();
		button1->setText("Button1");
		button1->setName("Button1");
		button1->setPosition(50, 50);
		button1->setSize(160, 45);
		button1->setActionListener([&] { switchToGameScreen(); });
		leftContainer->addChild(button1);

		shared_ptr<Button> button2 = make_shared<Button>();
		button2->setName("button2");
		button2->setName("Button2");
		button2->setPosition(50, 120);
		button2->setSize(160, 45);
		leftContainer->addChild(button2);

		shared_ptr<Button> button3 = make_shared<Button>();
		button3->setText("button3");
		button3->setName("Button3");
		button3->setPosition(50, 190);
		button3->setSize(160, 45);
		button3->setActionListener([&] { switchToTestScreen(); 	});
		leftContainer->addChild(button3);

		shared_ptr<Button> button4 = make_shared<Button>();
		button4->setText("button4");
		button4->setName("Button4");
		button4->setPosition(50, 260);
		button4->setSize(160, 45);
		button4->setActionListener([&] { switchToFooScreen(); 	});
		leftContainer->addChild(button4);

		shared_ptr<Button> button5 = make_shared<Button>();
		button5->setText("button5");
		button5->setName("Button5");
		button5->setPosition(50, 330);
		button5->setSize(160, 45);
		leftContainer->addChild(button5);
				
		linearContainter->addChild(leftContainer);
		linearContainter->setHorizontalGrowth(leftContainer, false);
		linearContainter->setVerticalGrowth(leftContainer, true);

		/// Populate right container
		shared_ptr<VBoxContainer> rightContainter = make_shared<VBoxContainer>();
		rightContainter->setName("rightContainer");
		rightContainter->setSpacing(10);

		shared_ptr<Button> rButton1 = make_shared<Button>();
		shared_ptr<Button> rButton2 = make_shared<Button>();
		shared_ptr<Button> rButton3 = make_shared<Button>();

		rButton1->setText("rButton1");
		rButton2->setText("rButton2");
		rButton3->setText("rButton3");

		rightContainter->addChild(rButton1);
		rightContainter->setVerticalGrowth(rButton1, true);
		rightContainter->setHorizontalGrowth(rButton1, true);
		
		rightContainter->addChild(rButton2);
		rightContainter->setVerticalGrowth(rButton2, true);
		rightContainter->setHorizontalGrowth(rButton2, true);
		
		rightContainter->addChild(rButton3);
		rightContainter->setVerticalGrowth(rButton3, true);
		rightContainter->setHorizontalGrowth(rButton3, true);
				
		linearContainter->addChild(rightContainter);
		linearContainter->setHorizontalGrowth(rightContainter, true);
		linearContainter->setVerticalGrowth(rightContainter, true);

		this->setRoot(linearContainter);
}


void
MainScreen::switchToTestScreen(){
	if  (about_screen != nullptr){
			application->set_screen(about_screen);
		}
}

void
MainScreen::switchToGameScreen(){
	if  (game_list_screen != nullptr){
		application->set_screen(game_list_screen);
	}
}

void
MainScreen::switchToFooScreen(){
	if  (foo_screen != nullptr){
		application->set_screen(foo_screen);
	}
}

void 
MainScreen::onBackPressed(){
    if (this->parent != nullptr){
        this->application->set_screen(parent);
    }
}



