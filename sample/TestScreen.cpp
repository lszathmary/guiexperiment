
#include "TestScreen.h"
#include "VBoxContainer.h"
#include "HBoxContainer.h"
#include "Label.h"
#include "Button.h"
#include "Application.h"
#include "ListView.h"
#include "ListItem.h"

TestScreen::TestScreen(shared_ptr<Application> application)
    :Screen(application->graphics),
    application(application) {

}

void
TestScreen::init(){
    shared_ptr<VBoxContainer> mainContainer = make_shared<VBoxContainer>();
    shared_ptr<Container> topContainer = make_shared<Container>();
    topContainer->setHeight(50);
    topContainer->setWidth(50);
    mainContainer->addChild(topContainer);
    mainContainer->setHorizontalGrowth(topContainer, true);
    //mainContainer->setVerticalGrowth(topContainer, true);

    shared_ptr<Label> label = make_shared<Label>();
    label->setText("Please select the files to import");
    label->setPosition(100, 0);
    label->setSize(300, 50);
    topContainer->addChild(label);

    shared_ptr<HBoxContainer> bottomContainer(new HBoxContainer());
    mainContainer->addChild(bottomContainer);
    mainContainer->setHorizontalGrowth(bottomContainer, true);
    mainContainer->setVerticalGrowth(bottomContainer, true);

    shared_ptr<VBoxContainer> leftContainer(new VBoxContainer());
    leftContainer->setPosition(0, 0);
    leftContainer->setWidth(200);
    bottomContainer->addChild(leftContainer);
    bottomContainer->setVerticalGrowth(leftContainer, true);


    


    shared_ptr<Button> btAddFolder = make_shared<Button>();
    btAddFolder->setText("Add Folder");
    btAddFolder->setPosition(60, 110);
    btAddFolder->setSize(160, 45);
    leftContainer->addChild(btAddFolder);

    shared_ptr<Button> btAddFile = make_shared<Button>();
    btAddFile->setText("Add File");
    btAddFile->setPosition(60, 180);
    btAddFile->setSize(160, 45);
    leftContainer->addChild(btAddFile);

    shared_ptr<Button> btRemove = make_shared<Button>();
    btRemove->setText("Remove");
    btRemove->setPosition(60, 250);
    btRemove->setSize(160, 45);
    leftContainer->addChild(btRemove);

    


    shared_ptr<Button> btRemoveAll = make_shared<Button>();
    btRemoveAll->setText("Remove All");
    btRemoveAll->setPosition(60, 320);
    btRemoveAll->setSize(160, 45);
    leftContainer->addChild(btRemoveAll);

    shared_ptr<Button> lbTest01 = make_shared<Button>();
    lbTest01->setText("List Item 01");
    

    /*
    shared_ptr<Button> lbTest02 = make_shared<Button>();
    lbTest02->setText("List Item 02 ");

    shared_ptr<Button> lbTest03 = make_shared<Button>();
    lbTest03->setText("List Item 03 ");

    shared_ptr<Button> btTest04 = make_shared<Button>();
    btTest04->setText("List Item 05 ");
   
   shared_ptr<Button> btTest05 = make_shared<Button>();
    btTest05->setText("List Item 05 ");

    shared_ptr<Button> btTest06 = make_shared<Button>();
    btTest06->setText("List Item 06 ");

    shared_ptr<Button> btTest07 = make_shared<Button>();
    btTest07->setText("List Item 07 ");
    */

    shared_ptr<HBoxContainer> rightContainer(new HBoxContainer());
    rightContainer->setPosition(0, 0);
    bottomContainer->addChild(rightContainer);
    bottomContainer->setHorizontalGrowth(rightContainer, true);
    bottomContainer->setVerticalGrowth(rightContainer, true);
    
    shared_ptr<ListView> listView = make_shared<ListView>();

    listView->setPosition(300, 110);
    listView->setSize(700, 500);
    //listView->styleRef().setBorderColor(Color(255, 0, 0));

    for (int idx = 0; idx < 25; idx++){
        //shared_ptr<Label> lable = make_shared<Label>();
        //lable->setText("C:\\Home\\Folder\\lable" + std::to_string(idx));

        shared_ptr<ListItem> lable = make_shared<ListItem>();
        //shared_ptr<Button> lable = make_shared<Button>();
        lable->setText("C:\\Games\\Folder" + std::to_string(idx));
        //lable->styleRef().setBorderColor(Color(255, 255, 255, 0));
        //lable->styleRef().setBackgroundColor(Color(255, 255, 255, 0));
        //lable->styleRef().setBorderColor(Color(0, 170, 0));
        //lable->styleRef().setBorderColor(Color(255, 255, 255, 255));
        listView->addChild(lable);
    }
    
    rightContainer->addChild(listView);
    rightContainer->setVerticalGrowth(listView, true);
    rightContainer->setHorizontalGrowth(listView, true);

    btAddFile->setActionListener([=] {
        auto item = listView->childAt(0);
        listView->scrollTo(item);
    });

    btRemove->setActionListener([=] {
        auto item = listView->childAt(10);
        listView->scrollTo(item);
    });

    this->setRoot(mainContainer);
}

void TestScreen::onBackPressed() {
    if (this->relative_screen != nullptr){
        this->application->set_screen(relative_screen);
    }
}



