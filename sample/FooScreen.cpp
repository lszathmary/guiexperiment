
#include "FooScreen.h"
#include "VBoxContainer.h"
#include "HBoxContainer.h"
#include "Label.h"
#include "Button.h"
#include "Application.h"
#include "ListView.h"
#include "ListItem.h"


FooScreen::FooScreen(shared_ptr<Application> application)
    :Screen(application->graphics),
    application(application) {

}

void
FooScreen::init(){
    /*
    shared_ptr<VBoxScene> vBoxScene = std::make_shared<VBoxScene>();

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();
    shared_ptr<Button> button4 = make_shared<Button>();

    button1->setText("rButton1");
    button2->setText("rButton2");
    button3->setText("rButton3");
    button4->setText("rButton4");

    vBoxScene->addChild(button1);
    vBoxScene->addChild(button2);
    vBoxScene->addChild(button3);
    vBoxScene->addChild(button4);
    vBoxScene->setVerticalGrowth(button1, true);
    vBoxScene->setHorizontalGrowth(button1, true);
    vBoxScene->setVerticalGrowth(button2, true);
    vBoxScene->setHorizontalGrowth(button2, true);
    vBoxScene->setVerticalGrowth(button3, true);
    vBoxScene->setHorizontalGrowth(button3, true);
    vBoxScene->setVerticalGrowth(button4, true);
    vBoxScene->setHorizontalGrowth(button4, true);

    this->set_root(vBoxScene);
    */
    
    /*
    shared_ptr<LinearContainer> linearContainer(new LinearContainer());
    linearContainer->setOrientation(Orientation::Horizotnal);
    linearContainer->setPosition(0, 0);
    

    // Left Container
    shared_ptr<LinearContainer> leftContainer(new LinearContainer());
    leftContainer->setOrientation(Orientation::Vertical);
    leftContainer->setName("leftContainer");
    leftContainer->setWidth(400);
    linearContainer->addChild(leftContainer);
    linearContainer->setHorizontalGrowth(leftContainer, true);
    linearContainer->setVerticalGrowth(leftContainer, true);
    
    // Top Lef Container
    shared_ptr<LinearContainer> topLeftContainer(new LinearContainer());
    //topLeftContainer->styleRef().setBackgroundColor(Color(255, 0, 0, 255));
    leftContainer->addChild(topLeftContainer);
    leftContainer->setVerticalGrowth(topLeftContainer, true);
    leftContainer->setHorizontalGrowth(topLeftContainer, true);

    shared_ptr<Button> tlButton1 = make_shared<Button>();
    shared_ptr<Button> tlButton2 = make_shared<Button>();
    shared_ptr<Button> tlButton3 = make_shared<Button>();

    tlButton1->setText("tlButton1");
    tlButton2->setText("tlButton2");
    tlButton3->setText("tlButton3");

    topLeftContainer->addChild(tlButton1);
    topLeftContainer->setVerticalGrowth(tlButton1, true);
    topLeftContainer->setHorizontalGrowth(tlButton1, true);
    
    topLeftContainer->addChild(tlButton2);
    topLeftContainer->setVerticalGrowth(tlButton2, true);
    topLeftContainer->setHorizontalGrowth(tlButton2, true);
    
    topLeftContainer->addChild(tlButton3);
    topLeftContainer->setVerticalGrowth(tlButton3, true);
    topLeftContainer->setHorizontalGrowth(tlButton3, true);

    // Bottom Left Container
    shared_ptr<LinearContainer> bottomLeftContainer(new LinearContainer());
    //bottomLeftContainer->styleRef().setBackgroundColor(Color(0, 255, 0, 255));
    leftContainer->addChild(bottomLeftContainer);
    leftContainer->setVerticalGrowth(bottomLeftContainer, true);
    leftContainer->setHorizontalGrowth(bottomLeftContainer, true);

    shared_ptr<Button> blButton1 = make_shared<Button>();
    shared_ptr<Button> blButton2 = make_shared<Button>();
    shared_ptr<Button> blButton3 = make_shared<Button>();

    blButton1->setText("blButton1");
    blButton2->setText("blButton2");
    blButton3->setText("blButton3");

    bottomLeftContainer->addChild(blButton1);
    bottomLeftContainer->setVerticalGrowth(blButton1, true);
    bottomLeftContainer->setHorizontalGrowth(blButton1, true);
    
    bottomLeftContainer->addChild(blButton2);
    bottomLeftContainer->setVerticalGrowth(blButton2, true);
    bottomLeftContainer->setHorizontalGrowth(blButton2, true);
    
    bottomLeftContainer->addChild(blButton3);
    bottomLeftContainer->setVerticalGrowth(blButton3, true);
    bottomLeftContainer->setHorizontalGrowth(blButton3, true);
                         
    

    /// Populate right container
    shared_ptr<LinearContainer> rightContainter = make_shared<LinearContainer>();
    
    rightContainter->setName("rightContainer");
    rightContainter->setOrientation(Orientation::Vertical);
    rightContainter->setSpacing(10);

    linearContainer->addChild(rightContainter);
    linearContainer->setHorizontalGrowth(rightContainter, true);
    linearContainer->setVerticalGrowth(rightContainter, true);

    // Top Right 
    shared_ptr<LinearContainer> topRightContainer(new LinearContainer());
    //topRightContainer->styleRef().setBackgroundColor(Color(255, 255, 0, 255));
    rightContainter->addChild(topRightContainer);
    rightContainter->setVerticalGrowth(topRightContainer, true);
    rightContainter->setHorizontalGrowth(topRightContainer, true);

    shared_ptr<Button> trButton1 = make_shared<Button>();
    shared_ptr<Button> trButton2 = make_shared<Button>();
    shared_ptr<Button> trButton3 = make_shared<Button>();

    trButton1->setText("trButton1");
    trButton2->setText("trButton2");
    trButton3->setText("trButton3");

    topRightContainer->addChild(trButton1);
    topRightContainer->setVerticalGrowth(trButton1, true);
    topRightContainer->setHorizontalGrowth(trButton1, true);
    
    topRightContainer->addChild(trButton2);
    topRightContainer->setVerticalGrowth(trButton2, true);
    topRightContainer->setHorizontalGrowth(trButton2, true);
    
    topRightContainer->addChild(trButton3);
    topRightContainer->setVerticalGrowth(trButton3, true);
    topRightContainer->setHorizontalGrowth(trButton3, true);

    
    // Bottom Right
    shared_ptr<LinearContainer> bottomRightContainer(new LinearContainer());
    //bottomRightContainer->styleRef().setBackgroundColor(Color(0, 0, 0, 255));
    rightContainter->addChild(bottomRightContainer);
    rightContainter->setVerticalGrowth(bottomRightContainer, true);
    rightContainter->setHorizontalGrowth(bottomRightContainer, true);

    shared_ptr<Button> brButton1 = make_shared<Button>();
    shared_ptr<Button> brButton2 = make_shared<Button>();
    shared_ptr<Button> brButton3 = make_shared<Button>();

    brButton1->setText("brButton1");
    brButton2->setText("brButton2");
    brButton3->setText("brButton3");

    bottomRightContainer->addChild(brButton1);
    bottomRightContainer->setVerticalGrowth(brButton1, true);
    bottomRightContainer->setHorizontalGrowth(brButton1, true);
    
    bottomRightContainer->addChild(brButton2);
    bottomRightContainer->setVerticalGrowth(brButton2, true);
    bottomRightContainer->setHorizontalGrowth(brButton2, true);
    
    bottomRightContainer->addChild(brButton3);
    bottomRightContainer->setVerticalGrowth(brButton3, true);
    bottomRightContainer->setHorizontalGrowth(brButton3, true);

    this->set_root(linearContainer);
    */


    
    shared_ptr<HBoxContainer> linearContainer(new HBoxContainer());
    linearContainer->setPosition(0, 0);
    

    // Populate left container
    shared_ptr<HBoxContainer> leftContainer(new HBoxContainer());
    leftContainer->setName("leftContainer");
    leftContainer->setWidth(400);
    
            
    shared_ptr<Button> button1 = make_shared<Button>();
    button1->setText("lButton1");
    button1->setPosition(50, 50);
    button1->setSize(160, 45);
    leftContainer->addChild(button1);
    leftContainer->setVerticalGrowth(button1, true);
    leftContainer->setHorizontalGrowth(button1, true);

    shared_ptr<Button> button2 = make_shared<Button>();
    button2->setName("lButton2");
    button2->setPosition(50, 120);
    button2->setSize(160, 45);
    leftContainer->addChild(button2);
    leftContainer->setVerticalGrowth(button2, true);
    leftContainer->setHorizontalGrowth(button2, true);

    shared_ptr<Button> button3 = make_shared<Button>();
    button3->setText("lButton3");
    button3->setPosition(50, 190);
    button3->setSize(160, 45);
    
    leftContainer->addChild(button3);
     leftContainer->setVerticalGrowth(button3, true);
    leftContainer->setHorizontalGrowth(button3, true);
    
            
    linearContainer->addChild(leftContainer);
    linearContainer->setHorizontalGrowth(leftContainer, true);
    linearContainer->setVerticalGrowth(leftContainer, true);

    /// Populate right container
    shared_ptr<VBoxContainer> rightContainter = make_shared<VBoxContainer>();
    
    rightContainter->setName("rightContainer");
    rightContainter->setSpacing(10);

    shared_ptr<Button> rButton1 = make_shared<Button>();
    shared_ptr<Button> rButton2 = make_shared<Button>();
    shared_ptr<Button> rButton3 = make_shared<Button>();

    rButton1->setText("rButton1");
    rButton2->setText("rButton2");
    rButton3->setText("rButton3");

    rightContainter->addChild(rButton1);
    rightContainter->setVerticalGrowth(rButton1, true);
    rightContainter->setHorizontalGrowth(rButton1, true);
    
    rightContainter->addChild(rButton2);
    rightContainter->setVerticalGrowth(rButton2, true);
    rightContainter->setHorizontalGrowth(rButton2, true);
    
    rightContainter->addChild(rButton3);
    rightContainter->setVerticalGrowth(rButton3, true);
    rightContainter->setHorizontalGrowth(rButton3, true);
            
    linearContainer->addChild(rightContainter);
    linearContainer->setHorizontalGrowth(rightContainter, true);
    linearContainer->setVerticalGrowth(rightContainter, true);



    this->setRoot(linearContainer);
    
}

void FooScreen::onBackPressed() {
    if (this->relative_screen != nullptr){
        this->application->set_screen(relative_screen);
    }
}



