#pragma once


#include "Screen.h"

class Application;

using namespace std;

class TestScreen :
	public inheritable_shared_from_this<TestScreen>, public Screen {

public:
    TestScreen(shared_ptr<Application> application);


    
    void init();
    virtual void onBackPressed() override;

    void set_parent(shared_ptr<Screen> screen){
        this->relative_screen = screen;
    }

protected:
    shared_ptr<Application> application;
    shared_ptr<Screen> relative_screen;
};