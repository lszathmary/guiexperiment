#pragma once


#include "Screen.h"
#include <iostream>
class Application;

using namespace std;

class GameListScreen :
	public inheritable_shared_from_this<GameListScreen>, public Screen {

public:
    GameListScreen(shared_ptr<Application> application);

    void init();
    virtual void onBackPressed() override;

    vector<string> load_cover_list();

    void set_parent(shared_ptr<Screen> screen){
        this->parent = screen;
    }

protected:
    shared_ptr<Application> application;
    shared_ptr<Screen> parent;
};