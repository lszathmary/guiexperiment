#include <SDL.h>
#include <SDL_ttf.h>
#include <iostream>

#include <memory>


#include "Widget.h"
#include "Container.h"
#include "Screen.h"
#include "Graphics.h"
#include "Application.h"
#include "Button.h"
#include "Label.h"
//#include "LinearContainer.h"
#include "ImageView.h"

#include "Animation.h"
#include "GridView.h"
#include "MainScreen.h"
#include "TestScreen.h"
#include "GameListScreen.h"
#include "FooScreen.h"

#include <tinyxml2.h>
using namespace std;
using namespace tinyxml2;

struct GuiState {
	int mousex;
	int mousey;
	int mousedown;
	bool active = true;
};


vector<string> load_cover_list() {
	std::vector<string> cover_list;
	XMLDocument doc;
	XMLError result = doc.LoadFile("data/test.xml");
	auto game_list = doc.FirstChildElement("game_list");
	auto game = game_list->FirstChildElement("game");

	while (game != nullptr) {
		const char* value;
		game->QueryStringAttribute("cover", &value);

		std::string s = value;
		cover_list.push_back(s);

		game = game->NextSiblingElement("game");
		cout << value << endl;
	};

	return cover_list;
}

int main(int argc, char* args[]){
	
	shared_ptr<Application> application(new Application());
	// TODO:
	// check vector order
	// fix warning
	// kaban test
	// add ui testing
	// add git ignore
	//
	// add option to deactivate mouse
	// glow highligt
	// texture in memory store
	// fix animation
	// sdl fsml alternative
	//
	// mixin clip rectangle only draw if child is inside parent
	// fix scale should resize instead
	// Imporive paddinge
	// pop/push clip rect
	// Image loading
	// Text aligment
	// add list view
	// pass event to childer(scroll)



	if (application->init()) {

		auto graphics = application->graphics;
		shared_ptr<Texture> textureFoo = make_shared<Texture>(graphics);
		textureFoo->loadFromFile("foo.jps");
		graphics->setRenderColor(Color(0, 0, 0));
		graphics->clear();
		graphics->setRenderColor(Color(0, 100, 0));
		graphics->fillRectangle(Rectangle(10, 10, 200, 200));
		graphics->present();

		//cin.get();
		shared_ptr<MainScreen> main_screen = make_shared<MainScreen>(application);
		shared_ptr<TestScreen> test_screen = make_shared<TestScreen>(application);
		shared_ptr<GameListScreen> game_list_screen = make_shared<GameListScreen>(application);
		shared_ptr<FooScreen> foo_screen = make_shared<FooScreen>(application);

		main_screen->init();
		test_screen->init();
		game_list_screen->init();
		foo_screen->init();

		main_screen->set_game_list_screen(game_list_screen);
		main_screen->set_about_screen(test_screen);
		main_screen->set_foo_screen(foo_screen);

		test_screen->set_parent(main_screen);
		game_list_screen->set_parent(main_screen);

		foo_screen->set_parent(main_screen);

		application->set_screen(main_screen);

			
		// -------------------------------------------------
		application->run();
		application->close();
	}

	return 0;

}
