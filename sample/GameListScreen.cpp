
#include "GameListScreen.h"
#include "Container.h"
#include "Label.h"
#include "Button.h"
#include "Application.h"
#include "GridView.h"
#include <tinyxml2.h>
#include <iostream>
using namespace std;
using namespace tinyxml2;


GameListScreen::GameListScreen(shared_ptr<Application> application)
    :Screen(application->graphics),
    application(application) {

}

void
GameListScreen::init(){
    auto cover_list = load_cover_list();

    shared_ptr<Texture> gradient = application->load_texture("data/images/panel_background.png");
    shared_ptr<Texture> texture = application->load_texture("data/images/bg7.png",10);

    shared_ptr<GridView> grid_view(new GridView());
    grid_view->setPosition(0, 0);
    grid_view->setSize(1280, 800);
    grid_view->set_gradient_bg(gradient);

    for (auto item : cover_list) {
        shared_ptr<GridItem> grid_item(new GridItem());
        shared_ptr<Texture> gameTexture = application->load_texture(item);
        //shared_ptr<Image> image2(new Image());

        //image2->loadFromFile(item);
        grid_item->setBorderTexture(texture);
        grid_item->setName(item);
        grid_item->setSize(205, 275);
        grid_item->setForegroundSpacing(6);
        grid_item->setTexture(gameTexture);
        //grid_item->setImage(image2);
        grid_view->addChild(grid_item);
    }

    setRoot(grid_view);
}

void GameListScreen::onBackPressed() {
    if (parent != nullptr){
        application->set_screen(parent);
    }
}


vector<string> GameListScreen:: load_cover_list() {
	std::vector<string> cover_list;
	XMLDocument doc;
	XMLError result = doc.LoadFile("data/test.xml");
	auto game_list = doc.FirstChildElement("game_list");
	auto game = game_list->FirstChildElement("game");

	while (game != nullptr) {
		const char* value;
		game->QueryStringAttribute("cover", &value);

		std::string s = value;
		cover_list.push_back(s);

		game = game->NextSiblingElement("game");
		cout << value << endl;
	};

	return cover_list;
}
