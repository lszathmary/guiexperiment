
#include <catch.hpp>
#include "HBoxContainer.h"
#include "Button.h"
#include "Screen.h"

TEST_CASE("HBoxContainer::update()", "[HBoxContainer.test.cpp]"){
    shared_ptr<HBoxContainer> hBoxContainer = make_shared<HBoxContainer>();
    hBoxContainer->setPosition(0, 0);
    hBoxContainer->setSize(100, 100);
    hBoxContainer->setSpacing(10);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();

    button1->setSize(50, 70);
    button2->setSize(50, 70);

    hBoxContainer->addChild(button1);
    hBoxContainer->setVerticalGrowth(button1, true);
    hBoxContainer->addChild(button2);
    hBoxContainer->setVerticalGrowth(button2, true);

    hBoxContainer->update();

    REQUIRE(button1->height() == 80);
    REQUIRE(button1->width() == 50);
    REQUIRE(button2->height() == 80);
    REQUIRE(button2->width() == 50);
}

TEST_CASE("HBoxContainer::onChangeFocus() - Direction::Right", "[HBoxContainer.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<HBoxContainer> hBoxContainer = make_shared<HBoxContainer>();

    screen->setRoot(hBoxContainer);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();

    hBoxContainer->addChild(button1);
    hBoxContainer->addChild(button2);
    hBoxContainer->addChild(button3);

    hBoxContainer->onChangeFocus(Direction::Right, button2);

    REQUIRE(button3->hasFocus() == true);
}

TEST_CASE("HBoxContainer::onChangeFocus() - Direction::Left", "[HBoxContainer.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<HBoxContainer> hBoxContainer = make_shared<HBoxContainer>();

    screen->setRoot(hBoxContainer);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();

    hBoxContainer->addChild(button1);
    hBoxContainer->addChild(button2);
    hBoxContainer->addChild(button3);

    hBoxContainer->onChangeFocus(Direction::Left, button2);

    REQUIRE(button1->hasFocus() == true);
}

TEST_CASE("HBoxContainer::onChangeFocus() - Direction::Up", "[HBoxContainer.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<HBoxContainer> hBoxContainer = make_shared<HBoxContainer>();

    screen->setRoot(hBoxContainer);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();

    hBoxContainer->addChild(button1);
    hBoxContainer->addChild(button2);
    hBoxContainer->addChild(button3);

    hBoxContainer->onChangeFocus(Direction::Up, nullptr);
    hBoxContainer->onChangeFocus(Direction::Up, button1);

    REQUIRE(button1->hasFocus() == true);
}

TEST_CASE("HBoxContainer::onChangeFocus() - Direction::Down", "[HBoxContainer.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<HBoxContainer> hBoxContainer = make_shared<HBoxContainer>();

    screen->setRoot(hBoxContainer);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();

    hBoxContainer->addChild(button1);
    hBoxContainer->addChild(button2);
    hBoxContainer->addChild(button3);

    hBoxContainer->onChangeFocus(Direction::Down, nullptr);
    hBoxContainer->onChangeFocus(Direction::Down, button1);

    REQUIRE(button1->hasFocus() == true);
}

TEST_CASE("HBoxContainer::onChangeFocus() - First Child", "[HBoxContainer.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<HBoxContainer> hBoxContainer = make_shared<HBoxContainer>();

    screen->setRoot(hBoxContainer);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();

    hBoxContainer->addChild(button1);
    hBoxContainer->addChild(button2);
    hBoxContainer->addChild(button3);

    hBoxContainer->onChangeFocus(Direction::Down, nullptr);

    REQUIRE(button1->hasFocus() == true);
}