#include <catch.hpp>
#include "VBoxContainer.h"
#include "Button.h"
#include "Screen.h"

TEST_CASE("VBoxContainer::update()", "[VBoxContainer.test.cpp]"){
    shared_ptr<VBoxContainer> vBoxContainer = make_shared<VBoxContainer>();
    vBoxContainer->setPosition(0, 0);
    vBoxContainer->setSize(100, 100);
    vBoxContainer->setSpacing(10);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();

    button1->setSize(50, 70);
    button2->setSize(50, 70);

    vBoxContainer->addChild(button1);
    vBoxContainer->setVerticalGrowth(button1, true);
    vBoxContainer->addChild(button2);
    vBoxContainer->setVerticalGrowth(button2, true);

    vBoxContainer->update();

    REQUIRE(button1->height() == 35);
    REQUIRE(button1->width() == 50);
    REQUIRE(button2->height() == 35);
    REQUIRE(button2->width() == 50);
}

TEST_CASE("VBoxContainer::onChangeFocus() - Direction::Up", "[HBoxContainer.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<VBoxContainer> vBoxContainer = make_shared<VBoxContainer>();

    screen->setRoot(vBoxContainer);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();

    vBoxContainer->addChild(button1);
    vBoxContainer->addChild(button2);
    vBoxContainer->addChild(button3);

    vBoxContainer->onChangeFocus(Direction::Up, button2);

    REQUIRE(button1->hasFocus() == true);
}

TEST_CASE("VBoxContainer::onChangeFocus() - Direction::Down", "[HBoxContainer.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<VBoxContainer> vBoxContainer = make_shared<VBoxContainer>();

    screen->setRoot(vBoxContainer);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();

    vBoxContainer->addChild(button1);
    vBoxContainer->addChild(button2);
    vBoxContainer->addChild(button3);

    vBoxContainer->onChangeFocus(Direction::Down, button2);

    REQUIRE(button3->hasFocus() == true);
}

TEST_CASE("VBoxContainer::onChangeFocus() - Direction::Left", "[HBoxContainer.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<VBoxContainer> vBoxContainer = make_shared<VBoxContainer>();

    screen->setRoot(vBoxContainer);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();

    vBoxContainer->addChild(button1);
    vBoxContainer->addChild(button2);
    vBoxContainer->addChild(button3);

    vBoxContainer->onChangeFocus(Direction::Left, nullptr);
    vBoxContainer->onChangeFocus(Direction::Left, button1);

    REQUIRE(button1->hasFocus() == true);
}

TEST_CASE("VBoxContainer::onChangeFocus() - Direction::Right", "[HBoxContainer.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<VBoxContainer> vBoxContainer = make_shared<VBoxContainer>();

    screen->setRoot(vBoxContainer);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();

    vBoxContainer->addChild(button1);
    vBoxContainer->addChild(button2);
    vBoxContainer->addChild(button3);

    vBoxContainer->onChangeFocus(Direction::Right, nullptr);
    vBoxContainer->onChangeFocus(Direction::Right, button1);

    REQUIRE(button1->hasFocus() == true);
}

TEST_CASE("VBoxContainer::onChangeFocus() - First Child", "[HBoxContainer.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<VBoxContainer> vBoxContainer = make_shared<VBoxContainer>();

    screen->setRoot(vBoxContainer);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();

    vBoxContainer->addChild(button1);
    vBoxContainer->addChild(button2);
    vBoxContainer->addChild(button3);

    vBoxContainer->onChangeFocus(Direction::Down, nullptr);

    REQUIRE(button1->hasFocus() == true);
}