#include <catch.hpp>
#include "ListView.h"
#include "ListItem.h"


TEST_CASE("ListView::update()", "[listView.test.cpp]"){
    std::shared_ptr<ListView> listView = std::make_shared<ListView>();

    std::shared_ptr<ListItem> item1 = std::make_shared<ListItem>();
    std::shared_ptr<ListItem> item2 = std::make_shared<ListItem>();

    listView->setPosition(50, 50);
    listView->setSize(400, 400);
    listView->setItemHeight(30);

    listView->addChild(item1);
    listView->addChild(item2);

    listView->update();

    REQUIRE(item1->height() == listView->itemHeight());
    REQUIRE(item1->width() == listView->contentBounds().width);

    REQUIRE(item2->height() == listView->itemHeight());
    REQUIRE(item2->width() == listView->contentBounds().width);
}

TEST_CASE("ListView::update() with ScrollBar", "[listView.test.cpp]"){
    std::shared_ptr<ListView> listView = std::make_shared<ListView>();

    listView->setPosition(50, 50);
    listView->setSize(400, 400);
    listView->setItemHeight(50);

    for (int idx = 0; idx < 10; idx++){
        std::shared_ptr<ListItem> item = std::make_shared<ListItem>();
        listView->addChild(item);
    }

    listView->update();

    const auto scrollBarWidth = 5;

    REQUIRE(listView->childAt(0)->height() == listView->itemHeight());
    REQUIRE(listView->childAt(0)->width() == listView->contentBounds().width - scrollBarWidth);
}