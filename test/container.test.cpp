#include <catch.hpp>
#include "Container.h"
#include "Button.h"
#include "Screen.h"

TEST_CASE("Container::onChangeFocus() - First Child", "[container.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<Container> container = make_shared<Container>();

    screen->setRoot(container);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();

    container->addChild(button1);
    container->addChild(button2);

    container->onChangeFocus(Direction::Up, nullptr);

    REQUIRE(button1->hasFocus() == true);
}

TEST_CASE("Container::onChangeFocus() - Direction::Up", "[container.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<Container> container = make_shared<Container>();

    screen->setRoot(container);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();

    container->addChild(button1);
    container->addChild(button2);

    container->onChangeFocus(Direction::Up, button2);

    REQUIRE(button1->hasFocus() == true);
}

TEST_CASE("Container::onChangeFocus() - Direction::Up - Nearest Focus Index", "[Container.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<Container> container = make_shared<Container>();

    screen->setRoot(container);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();
    shared_ptr<Button> button4 = make_shared<Button>();

    container->addChild(button1);
    container->addChild(button2);
    container->addChild(button3);
    container->addChild(button4);
    
    button1->setFocusIndex(5);
    button2->setFocusIndex(6);
    button3->setFocusIndex(7);
    button4->setFocusIndex(8);

    container->onChangeFocus(Direction::Up, button3);

    REQUIRE(button2->hasFocus() == true);
}

TEST_CASE("Container::onChangeFocus() - Direction::Down ", "[Container.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<Container> container = make_shared<Container>();

    screen->setRoot(container);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();

    container->addChild(button1);
    container->addChild(button2);

    container->onChangeFocus(Direction::Down, button1);

    REQUIRE(button2->hasFocus() == true);
}

TEST_CASE("Container::onChangeFocus() - Direction::Down - Nearest Focus Index", "[Container.test.cpp]"){
    shared_ptr<Screen> screen = make_shared<Screen>(nullptr);
    shared_ptr<Container> container = make_shared<Container>();

    screen->setRoot(container);

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();
    shared_ptr<Button> button4 = make_shared<Button>();

    container->addChild(button1);
    container->addChild(button2);
    container->addChild(button3);
    container->addChild(button4);

    button1->setFocusIndex(6);
    button2->setFocusIndex(5);
    button3->setFocusIndex(8);
    button4->setFocusIndex(7);

    container->onChangeFocus(Direction::Down, button1);
    
    REQUIRE(button4->hasFocus() == true);
}

TEST_CASE("Container::setFocusIndex() after addChild()", "[Container.test.cpp]"){
    shared_ptr<Container> container = make_shared<Container>();

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();

    container->addChild(button1);
    container->addChild(button2);

    shared_ptr<Button> button3 = make_shared<Button>();
    
    container->addChild(button3);
    button3->setFocusIndex(1);

    REQUIRE(button1->focusIndex() == 0);
    REQUIRE(button2->focusIndex() == 3);
    REQUIRE(button3->focusIndex() == 1);
}


TEST_CASE("Container::setFocusIndex() - Already in Use", "[Container.test.cpp]"){
    shared_ptr<Container> container = make_shared<Container>();

    shared_ptr<Button> button1 = make_shared<Button>();
    shared_ptr<Button> button2 = make_shared<Button>();
    shared_ptr<Button> button3 = make_shared<Button>();

    container->addChild(button1);
    container->addChild(button2);
    container->addChild(button3);
    
    button3->setFocusIndex(0);
    button2->setFocusIndex(1);
    button1->setFocusIndex(2);

    REQUIRE(button1->focusIndex() == 2);
    REQUIRE(button2->focusIndex() == 1);
    REQUIRE(button3->focusIndex() == 0);
}


TEST_CASE("Container::addChild() Twice", "[Container.test.cpp]"){
    shared_ptr<Container> container = make_shared<Container>();
    shared_ptr<Button> button = make_shared<Button>();

    container->addChild(button);

    REQUIRE_THROWS(container->addChild(button));
}

TEST_CASE("Container::removeChild() Oprhan", "[Container.test.cpp]"){
    shared_ptr<Container> container = make_shared<Container>();
    shared_ptr<Button> button = make_shared<Button>();

    REQUIRE_THROWS(container->removeChild(button));
}
