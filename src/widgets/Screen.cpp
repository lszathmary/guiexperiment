#include "Screen.h"
#include <iostream>

Screen::Screen(std::shared_ptr<Graphics> graphics)
	:height(300),
	width(300),
	mFocusedWidget(nullptr) {
	
	this->mRootWidget = std::shared_ptr<Widget>(new Widget());
	
	this->mRootWidget->setPosition(0, 0);
	this->mRootWidget->setSize(width, height);
	
	Style style;
	//style.backgroundColor = { 255,0, 0, 0 };
	this->mRootWidget->setStyle(style);
	
}

void Screen::onMouseDown(int x, int y) {
	auto widget = mRootWidget->findWidgetAt(x, y);

	if (widget != nullptr) {
		mPressedWidget = widget;
		widget->onMouseDown(x, y);
	}

}

void Screen::onMouseUp(int x, int y){
	if (mPressedWidget != nullptr) {
		mPressedWidget->onMouseUp(x, y);
		mPressedWidget = nullptr;
	}
}

void Screen::onMouseMove(int x, int y) {
	auto widget = mRootWidget->findWidgetAt(x, y);

	if (widget != nullptr) {
		widget->onMouseMove(x, y);
	}

	changeFocusTo(widget);

	mouse_position.x = x;
	mouse_position.y = y;
}

void Screen::changeFocusTo(std::shared_ptr<Widget> widget) {
	if (widget == mFocusedWidget) 
		return;
	
	if (mFocusedWidget != nullptr) {
		mFocusedWidget->setFocus(false);
		mFocusedWidget = nullptr;
	}

	if (widget != nullptr) {
		if (widget->isFocusTraversable()) {
			mFocusedWidget = widget;
			mFocusedWidget->setFocus(true);
		}
	}
}

void Screen::onMouseScroll(int x , int y, int delta) {
	// Does not work layout is not update.
	if (mRootWidget->contains(x, y)) {
		mRootWidget->onMouseScroll(x, y, delta);
	}
}

void Screen::onMoveNext() {
	if (mFocusedWidget != nullptr) {
		mFocusedWidget->onChangeFocus(Direction::Right, mFocusedWidget);
	}
	else {
		mRootWidget->onChangeFocus(Direction::Right, nullptr);
	}
}

void Screen::onMoveBack() {
	if (mFocusedWidget != nullptr) {
		mFocusedWidget->onChangeFocus(Direction::Left, mFocusedWidget);
	}
	else {
		mRootWidget->onChangeFocus(Direction::Left, nullptr);
	}
}

void Screen::omNoveUp() {
	if (mFocusedWidget != nullptr) {
		mFocusedWidget->onChangeFocus(Direction::Up, mFocusedWidget);
	}
	else {
		mRootWidget->onChangeFocus(Direction::Up, nullptr);
	}
}

void Screen::omMoveDown() {
	if (mFocusedWidget != nullptr) {
		mFocusedWidget->onChangeFocus(Direction::Down, mFocusedWidget);
	}
	else {
		mRootWidget->onChangeFocus(Direction::Down, nullptr);
	}
}

void Screen::requestFocus(std::shared_ptr<Widget> widget) {
	if (mFocusedWidget != nullptr) {
		mFocusedWidget->setFocus(false);
		mFocusedWidget = nullptr;
	}
	   
	if (widget != nullptr) {
		if (widget->isFocusTraversable()) {
			mFocusedWidget = widget;
			mFocusedWidget->setFocus(true);
		}
	}
}

void Screen::draw(std::shared_ptr<Graphics> graphics) {
	if (mRootWidget != nullptr) {
		mRootWidget->draw(graphics);
	}
}

void Screen::update() {
	//this->assure_focus();

	if (mRootWidget != nullptr) {
		mRootWidget->update();
	}
}

void Screen::setRoot(std::shared_ptr<Widget> widget) {
	mRootWidget = widget;

	mRootWidget->setScreen(shared_from_this());
	mRootWidget->setPosition(0, 0);
	mRootWidget->onChangeFocus(Direction::Down, nullptr);
}

void Screen::assureFocus() {
	auto widget = mRootWidget->findWidgetAt(mouse_position.x, mouse_position.y);
	changeFocusTo(widget);
}

void Screen::onActionPeformed(){
	if (mFocusedWidget != nullptr) {
		mFocusedWidget->onActionPerformed();
	}	
}

void Screen::onBackPressed(){
	
}

void Screen::onResize(int width, int height){
	width = width;
	height = height;

	if (mRootWidget != nullptr){
		mRootWidget->setSize(width, height);
	}
}