#pragma once

#include <string>
#include "SDL_image.h"
#include "Widget.h"
#include "Image.h"

using namespace std;

class ImageView :
	public inheritable_shared_from_this <Container>, public Widget
{
public:
	ImageView();
	void set_image(shared_ptr<Image> image);

	virtual void draw(shared_ptr<Graphics> graphics) override;

private:
	shared_ptr<Image> image;
};

