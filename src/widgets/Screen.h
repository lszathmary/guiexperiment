#pragma once

#include "Container.h"
#include "Graphics.h"

class Screen :
	 public inheritable_shared_from_this<Screen>
{
public:
	Screen(std::shared_ptr<Graphics> graphics);

	void setRoot(std::shared_ptr<Widget> root);
	std::shared_ptr<Widget> root() { return this->mRootWidget; }

	void requestFocus(std::shared_ptr<Widget> widget);

	virtual void onResize(int width, int height);

	virtual void onActionPeformed();

	virtual void onBackPressed();

	virtual void onMouseDown(int x, int y);

	virtual void onMouseUp(int x, int y);

	virtual void onMouseMove(int x, int y);

	virtual void onMouseScroll(int x, int y, int delta);
	
	virtual void onMoveNext();

	virtual void onMoveBack();

	virtual void omNoveUp();

	virtual void omMoveDown();
	
	virtual void update();

	virtual void draw(std::shared_ptr<Graphics> graphics);

private:

	// Assures that the correct widget has the focus.
	void assureFocus();

	void changeFocusTo(std::shared_ptr<Widget> widget);

private:
	Point mouse_position;

	int width;
	int height;

	std::shared_ptr<Widget> mPressedWidget;
	std::shared_ptr<Widget> mFocusedWidget;
	std::shared_ptr<Widget> mRootWidget;

	std::shared_ptr<Graphics> mGraphics;
};

