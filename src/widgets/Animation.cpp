#include "Animation.h"
#include "Widget.h"
#include <algorithm>
#include "Timer.h"
#include "Utils.h"

Animation::Animation():
	start(0),
	duration(200),
	playing(false),
	scale(1.0),
	delta(0.2),
	rectange(50,50,100,100) {
	
}

void Animation::set_rectangle(Rectangle rectangle) {
	this->rectange = rectangle;
}

void Animation::update() {
	uint32_t now = getTimeInMilliSeconds();
	uint32_t elapsed =  now - start;

	if (this->playing) {
		if (elapsed > duration) {
			this->playing = false;
			//cout << "x: " << scaled_rectangle.x  << "y: " << scaled_rectangle.y << endl;
			//cout << "w: " << scaled_rectangle.width << "h: " << scaled_rectangle.height << endl;
		}
		else {
			double temp = (((double)elapsed / (double)duration) * (double)delta);
			double new_scale = this->scale + temp;

			//cout << new_scale << endl;

			// only works updwards
			int scaled_width = (int) (rectange.width * new_scale);
			int scaled_height = (int) (rectange.height * new_scale);

			int width_dif = scaled_width - rectange.width;
			int height_dif = scaled_width - rectange.width;

			int scaled_x = rectange.x - (width_dif / 2);
			int scaled_y = rectange.y - (height_dif / 2);

			scaled_rectangle = { scaled_x, scaled_y, scaled_width, scaled_height };
		}
	}
}

void Animation::play(){
	this->scale = 1.2;
	this->delta = 0.1;
	this->playing = true;
	this->start = getTimeInMilliSeconds();
}

ScaleAnimation::ScaleAnimation():
	playing(false),
	start_time(0),
	duration(200),
	start_bound(0, 0, 0 ,0 ),
	vertical_scale(1.1),
	horizontal_scale(1.1),
	widget(nullptr){

	//this->new_height = 260;
	//this->new_width = 190;

	this->new_height = 255;
	this->new_width = 180;
}

void ScaleAnimation::play() {
	
	
	this->playing = true;
	this->start_time = getTimeInMilliSeconds();
	this->start_bound = widget->bounds();



	//timer.set_interval(&(this->update), 1000)
	
	/*
	timer.set_interval([&]() {
		this->update();
		}, 25);
	*/

}

void ScaleAnimation::scale_widget(uint32_t elapsed) {
	zoom_widget(elapsed);
	// resize from 180 to 190
	
	/*
	double vertical_delta = 1.0 + ((double) elapsed / (double)duration) * (vertical_scale - 1.0);
	double horizontal_delta = 1.0 + ((double) elapsed / (double)duration) * (horizontal_scale - 1.0);

	int width = start_bound.width * vertical_delta;
	int height = start_bound.height * horizontal_delta;

	int width_difference = width - start_bound.width;
	int height_difference = height - start_bound.height;

	int x = start_bound.x - (width_difference / 2);
	int y = start_bound.y - (height_difference / 2);

	this->widget->set_bounds({ x, y, width, height });
	*/
}

void ScaleAnimation::zoom_widget(uint32_t elapsed) {
	int width_diff = new_width - start_bound.width;
	int height_diff = new_height - start_bound.height;

	int vertical_delta = (int)(((double)elapsed / (double)duration) * height_diff);
	int horizontal_delta = (int)(((double)elapsed / (double)duration) * width_diff);

	int width = start_bound.width + horizontal_delta;
	int height = start_bound.height + vertical_delta;

	int width_difference = width - start_bound.width;
	int height_difference = height - start_bound.height;

	//widget->get

	int x = start_bound.x - (width_difference / 2);
	int y = start_bound.y - (height_difference / 2);

	this->widget->setSize(width, height);
	//this->widget->set_bounds({ x, y, width, height });

}

void ScaleAnimation::update() {
	if (playing == false) return;
	
	uint32_t now = getTimeInMilliSeconds();
	uint32_t elapsed = now - start_time;
	
	this->scale_widget(std::min(elapsed,duration));

	if (elapsed > duration) {
		playing = false;
		this -> timer.stop();
	}
}
void ScaleAnimation::end() {
	timer.stop();
	this->scale_widget(duration);
	this->playing = false;
}

void ScaleAnimation::pause() {
	timer.stop();
	this->playing = false;
}