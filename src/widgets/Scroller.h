#pragma once
#include <cstdint>

class Scroller
{
public:
	Scroller();

	void scrollBy(int from, int amount, int min, int max);
	void update();

	int getValue() { return this->scroll_value;  }


private:
	bool is_scrolling;


	int min_scroll;
	int max_scroll;
	int scroll_start;
	int scroll_value;
	int scroll_amount;

	uint32_t start_time;
	uint32_t duration;

};

