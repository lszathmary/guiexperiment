#pragma once
#include "Common.h"
#include "Widget.h"
#include "Animation.h"

class GridItem : 
	public inheritable_shared_from_this<GridItem>, public Widget {

public:
	GridItem();

	std::shared_ptr<Texture> texture() { return mTexture; }
	void setTexture(std::shared_ptr<Texture> texture) { mTexture = texture; }

	int foregroundSpacing() { return mForegroundSpacing; }
	void setForegroundSpacing(int spacing) { mForegroundSpacing = spacing; }

	std::shared_ptr<Texture> borderTexture() { return mBorderTexture; }
	void setBorderTexture(std::shared_ptr<Texture> texture) { mBorderTexture = texture; }

	virtual void onFocusChanged(bool focused) override;

	virtual void onMouseScroll(int x, int y, int delta) override;

	virtual void draw(std::shared_ptr<Graphics> graphics) override;

	virtual void update() override;

private:
	// The space around the texture and the focus rectangle
	int mForegroundSpacing;
	std::shared_ptr<Texture> mBorderTexture;
	std::shared_ptr<Texture> mTexture;

	ScaleAnimation mAnimation;
};


