#pragma once

#include <memory>
#include <string>
#include <functional>

#include "Base.h"
#include "Style.h"
#include "Graphics.h"

class Container;
class Screen;

/**
 * Base class for all widgets.
 *  Should we use prefred width/height?
 */
class Widget 
	: public inheritable_shared_from_this<Widget>
{

public:
	/// Construct a new widget.
	Widget();
	
	/// Return the parent widget
	std::shared_ptr<Container> parent() { return mParent; }
	/// Set the parent widget
	void setParent(std::shared_ptr<Container> parent) { mParent = parent; }
	
	/// Return the name of the widget
	std::string name() { return mName; }
	/// Set the name of the widget
	void setName(std::string name) { mName = name; }

	/// Return the index specifing the focus order
	int	focusIndex() { return mFocusIndex; }
	// Set the index specifying the focus order
	void setFocusIndex(int focusIndex);
	
	/// Return the style used to draw the widget
	Style& styleRef() { return mStyle; }
	/// Return the style used to draw the widget
	Style style() { return mStyle; }
	/// Set the style usd to draw the widget	
	void setStyle(Style style) { mStyle = style; }

	/// Return the parent screen of the widget		
	std::shared_ptr<Screen> screen() { return mScreen; }
	/// Set the parent screen of the widget
	virtual void setScreen(std::shared_ptr<Screen> screen);

	/// Return the relative position of the widget
	Point position() { return Point{ mBounds.x, mBounds.y}; };
	///  Set the relative position of the widget
	void setPosition(int x, int y)	{ mBounds.x = x; mBounds.y = y;	}

	/// Return the size of the widget
	Size size() { return Size{ mBounds.width, mBounds.height}; }
	/// Set the size of the widget
	void setSize(int width, int height) { mBounds.width = width; mBounds.height = height; }
	
	/// Return the height of the widget
	int height() { return mBounds.height; }
	/// Set the height of the widget	
	void setHeight(int height) { mBounds.height = height; }

	/// Return the width of the widget
	int width() { return mBounds.width; }
	/// Set the width of the widget
	void setWidth(int width) { mBounds.width = width; }
	
	/// Return the absolute bounds of the widget
	Rectangle absoluteBounds() { return mAbsoluteBounds; }
	/// Return absolute bounds without border
	Rectangle innerBounds() { return mInnerBounds; }
	/// Return relative bounds without border
	Rectangle contentBounds() { return mContentBounds; }

	/// Return the relative bounds of the widget
	Rectangle bounds() { return mBounds; }
	/// Set the relative bounds of the widget
	void setBounds(Rectangle bounds) { mBounds = bounds; }
	
	/// Returns true if the widget has focus otherwiser returns false.
	bool hasFocus() { return this->mFocused; }
	/// Set whether or not this widget is currently focused
	void setFocus(bool focused) { this->mFocused = focused; onFocusChanged(focused); }

	/// Requests that this Node get the input focus.
	void requestFocus();

	/// Returns true if the can have focus otherwiser returns false
	virtual bool isFocusTraversable() { return true; }

	virtual bool isContainer() { return false; }

	// Determines if the widget contains the specified coordinates.
	bool contains(int x, int y);

	/// Assure that the specified index of the child is available
	virtual void assureAvailableFocusIndex(int index);
	
	/// Finds the widget at the specified location.
	virtual std::shared_ptr<Widget> findWidgetAt(int x, int y);

	/// Defines a function to be called when the focus has to change.
	virtual void onChangeFocus(Direction direction, std::shared_ptr<Widget> sender);

	/// Defines a function to be called when a mouse button is pressed.
	virtual void onMouseDown(int x, int y);

	/// Defines a function to be called when the mouse is moved.
	virtual void onMouseMove(int x, int y);

	/// Defines a function to be called when the mouse button is released
	virtual void onMouseUp(int x, int y);

	/// Defines a function to be called when the mouse is scrolled.
	virtual void onMouseScroll(int x, int y, int delta);
	
	/// Defines a function to be called when focus has changed.
	virtual void onFocusChanged(bool focused);

	/// Defines a function to be called when an action is performed.
	virtual void onActionPerformed();

	/// Update the layout of the widget.
	virtual void update();

	/// Draw the widget.
	virtual void draw(std::shared_ptr<Graphics> graphics);

	/// 
	std::function<void()> actionListener() { return mActionListener; }
	/// 
	virtual void setActionListener(std::function<void()> listener);

protected:
	
	// Initialize the widget.
	void initialize();

	// Calculate the absolute bounds of the widget
	Rectangle computeAbsoluteBounds();

	// Calculate the absolute bounds without border
	Rectangle computeInnerBounds();

	// Calculate the relative bounds without border
	Rectangle computeContentBounds();

protected:
	// The name of widget(uniqeness is not verifyed at the moment)
	std::string mName;
	// The style used for drawing the widget 
	Style mStyle;
	// Index indicating the focus order of the widget
	int mFocusIndex;
	// Indicates wether the widget has focus or not
	bool mFocused;

	// Relative bounds without border;
	Rectangle mContentBounds;
	// Absoule bounds of the widget
	Rectangle mAbsoluteBounds;
	// Absoule bounds without border
	Rectangle mInnerBounds;
	// Relative bounds of the widget
	Rectangle mBounds;

	// The screen that contains this widget
	std::shared_ptr<Screen> mScreen;
	// The parent of the widget
	std::shared_ptr<Container> mParent;

	// Listener to notify in case of an action occured
	std::function<void()> mActionListener;
};
