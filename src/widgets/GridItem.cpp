#include "GridItem.h"

GridItem::GridItem() {
	setWidth(180);
	setHeight(250);
		
	mStyle.setBorderColor(Color(200, 200, 200, 255));
}

void GridItem::draw(std::shared_ptr<Graphics> graphics) {
	auto absouleBoundsWithSpacing = Rectangle(mAbsoluteBounds);

	absouleBoundsWithSpacing.x += mForegroundSpacing;
	absouleBoundsWithSpacing.y += mForegroundSpacing;
	absouleBoundsWithSpacing.width -= 2 * mForegroundSpacing;
	absouleBoundsWithSpacing.height -= 2 * mForegroundSpacing;

	if (mBorderTexture != nullptr) {
		graphics->drawTexture(mAbsoluteBounds, *mBorderTexture);
	}

	if (mTexture != nullptr) {
		graphics->drawTexture(absouleBoundsWithSpacing, *mTexture);
	}

	if (mFocused) {
		graphics->setRenderColor(mStyle.borderColor());
		graphics->drawRectangle(absouleBoundsWithSpacing);
	}
}

void GridItem::update() {
	mAnimation.update();
	Widget::update();
}

void GridItem::onFocusChanged(bool focused) {
	auto widget = Widget::shared_from_this();
	
	mAnimation.set_widget(widget);
	if (focused) {
		if (mAnimation.is_playing()) {
			mAnimation.pause();
		}

		mAnimation.set_new_height(300);
		mAnimation.set_new_width(230);
		mAnimation.play();

		mStyle.setBorderColor(Color(200, 200, 200, 255));
	}
	else {
		if (mAnimation.is_playing()) {
			mAnimation.pause();
		}

		mAnimation.set_new_height(275);
		mAnimation.set_new_width(205);
		mAnimation.play();
		
		mStyle.setBorderColor(Color( 0, 0, 0 , 0));
	}
	
}

void GridItem::onMouseScroll(int x, int y, int delta) {
	// This mouse scroll event it is okay
	if (mAnimation.is_playing()) {
		mAnimation.end();
	}

	mAnimation.set_new_height(255);
	mAnimation.set_new_width(180);
}