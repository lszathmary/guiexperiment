#include "ImageView.h"

ImageView::ImageView() {
	this->mStyle.setBackgroundColor(Color( 50, 50, 50, 0));
	//this->bounds = { 0,0, 120,170 };
	this->mBounds = { 0,0, 180,255 };
}

void ImageView::set_image(shared_ptr<Image> image) {
	this->image = image;
}

void ImageView::draw(shared_ptr<Graphics> graphics) {
	Widget::draw(graphics);

	if (mFocused) {
		graphics->setRenderColor(mStyle.highlightColor());
		graphics->fillRectangle(mAbsoluteBounds);
	}

	if (image != nullptr) {
		//Rectangle rect = { this->bounds.x + 5, this->bounds.y + 5, this->bounds.width - 10, this->bounds.height -10  };
		Rectangle rect = { absoluteBounds().x + 5, absoluteBounds().y + 5,
			absoluteBounds().width - 10, absoluteBounds().height - 10 };
		graphics->drawImage(rect, *image);
	}

}