
#pragma once

#include "Container.h"
#include "Scroller.h"

/// does focus index work, vector order
/// Fix focus chane
/// Decide child widge type
class ListView: 
    public inheritable_shared_from_this <ListView>, public Container 
{
public:
    /// Construct a new ListView
    ListView();

    /// Return the height of list items
    int itemHeight() { return mItemHeight; }
    /// Sets the height of the list items
    void setItemHeight(int lineHeight) { mItemHeight = lineHeight; }

    virtual bool isFocusTraversable() override { return true; }

    virtual void onChangeFocus(Direction direction, std::shared_ptr<Widget> Widget) override;

    virtual void onMouseScroll(int x, int y, int delta) override;

    virtual void update() override;
    
    void draw(std::shared_ptr<Graphics> graphics);

    void scrollTo(std::shared_ptr<Widget> widget);

  
protected:

    /// Determines whether the scrollbar is visible or not.
    bool isScrollBarVisible();

    /// Draws the scrollbar
    void drawScrollBar(std::shared_ptr<Graphics> graphics);
   

private:

    int mScrollBarWidth;
    int mScrollOffset;
    int mItemHeight;
    int mFirstVisibleIndex;
    int mLastVisibleIndex;

    Scroller mScroller;
};