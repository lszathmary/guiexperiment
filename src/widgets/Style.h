#pragma once

#include "Common.h"

/**
 * Describes the apperance of the widget.
 */
class Style 
{
public:

	Color& backgroundColor() { return mBackgroundColor;  }
	void setBackgroundColor(Color& color) { mBackgroundColor = color; }

	Color& foregroundColor() { return mForegroundColor;  }
	void setForegroundColor(Color& color) { mForegroundColor = color; }

	Color& highlightColor() { return mHighlightColor; }
	void setHighlightColor(Color& color) { mHighlightColor = color; }

	Color& borderColor() { return mBorderColor;  }
	void setBorderColor(Color& color) { mBorderColor = color; }

	bool border() { return mBorder;  }
	void setBorder(bool enabled) { mBorder = enabled; }

	int borderWidth() { return mBorderWidth;  }
	void setBorderWidth(int borderWidth) { mBorderWidth = borderWidth; }

	int padding() { return mPadding; }
	void setPadding(int padding) { mPadding = padding; }

private:
	Color mBackgroundColor;
	Color mForegroundColor;
	Color mHighlightColor;
	Color mBorderColor;

	bool mBorder;
	int mBorderWidth = 1;
	int mPadding;
};
