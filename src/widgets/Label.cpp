#include "Label.h"
#include "Theme.h"

Label::Label() {
	mText = "label";
	mStyle.setBackgroundColor(Color(255, 255, 255,0));
	mStyle.setForegroundColor(Theme::colorOnPrimary);
	mStyle.setBorder(false);
} 

void Label::draw(shared_ptr<Graphics> graphics) {
	Widget::draw(graphics);

	graphics->setRenderColor(mStyle.foregroundColor());
	graphics->drawText(mAbsoluteBounds, mText);
}
