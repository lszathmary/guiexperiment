#include "Button.h"
#include "Theme.h"

Button::Button() {
	mText = "button";
	mStyle.setBackgroundColor(Theme::colorPrimaryDark);
	mStyle.setBorderColor(Theme::colorPrimaryLight);
	mStyle.setForegroundColor(Theme::colorOnPrimary);
	//mStyle.setHighlightColor(Theme::colorSecondary);
}

void Button::draw(shared_ptr<Graphics> graphics) {
	Widget::draw(graphics);
	
	if (mTexture != nullptr){
		graphics->drawTexture(mAbsoluteBounds, *mTexture);
	}

	if (mFocused) {
		graphics->setRenderColor(Theme::colorPrimaryLight);
		graphics->fillRectangle(mAbsoluteBounds);
	}

	graphics->setRenderColor(mStyle.foregroundColor());
	graphics->drawText(mAbsoluteBounds, mText);

	graphics->setRenderColor(mStyle.borderColor());
	graphics->drawRectangle(mAbsoluteBounds);

	/*
	if (mFocused) {
		graphics->setRenderColor(mStyle.highlightColor());
		graphics->drawRectangle(mAbsoluteBounds);
	}
	*/

}
