#include "Container.h"
#include <iostream>
#include "Graphics.h"
#include <algorithm>
#include "Theme.h"
#include <iostream>

Container::Container() {
	mStyle.setBackgroundColor(Theme::colorPrimary);
	mStyle.setBorder(false);
}

void Container::addChild(std::shared_ptr<Widget> child) {
	if (child->parent() != nullptr) {
		throw std::exception("The widget already has a parent");
	}

	mChildern.push_back(child);

	child->setParent(std::static_pointer_cast<Container>(Widget::shared_from_this()));
	child->setScreen(mScreen);

	// Assign focus index to the child, disregard old index.
	if (child->isFocusTraversable()){
		int focusIndex = findFirstAvailableFocusIndex();
		child->setFocusIndex(focusIndex);
	}
}

void Container::removeChild(std::shared_ptr<Widget> child){
	auto it = std::find(mChildern.begin(), mChildern.end(), child);

	if (it == mChildern.end()){
		throw std::exception("The widget is not a child of the container");
	}

	mChildern.erase(it);

	child->setParent(nullptr);
	child->setScreen(nullptr);
	child->setFocusIndex(-1);
}

void Container::update(){
	Widget::update();

	for (auto child : mChildern) {
		child->update();
	}
}

void Container::draw(std::shared_ptr<Graphics> graphics){
	// TODO: Clip items that are outside the bound rectangle.

	Widget::draw(graphics);

	if (mBackgroundTexture!= nullptr) {
		graphics->drawTexture(mAbsoluteBounds, *mBackgroundTexture);
	}

	for (auto child : mChildern) {
		child->draw(graphics);
	}
}

std::shared_ptr<Widget> Container::findWidgetAt(int x, int y) {
	for (auto child : mChildern) {
		auto widget = child->findWidgetAt(x,y);
		
		if (widget != nullptr) {
			return widget;
		}
	}

	return contains(x, y)
		? std::static_pointer_cast<Container>(Widget::shared_from_this())
		: nullptr;
}

int Container::findFirstAvailableFocusIndex(){
	int focusIndex = 0;

	for (auto child : this->mChildern) {
		if (focusIndex <= child->focusIndex()) {
			focusIndex = focusIndex + 1;
		}
	}

	return focusIndex;
}

void Container::setScreen(std::shared_ptr<Screen> screen) {
	mScreen = screen;

	for (auto child : mChildern){
		child->setScreen(screen);
	}
}

void Container::onMouseScroll(int x, int y, int delta) {
	for (auto child : mChildern) {
		if (child->contains(x, y)) {
			child->onMouseScroll(x, y, delta);
		}
	}
}

void Container::onChangeFocus(Direction direction, std::shared_ptr<Widget> child) {
	if (mChildern.empty()){
		return;
	}

	if (child == nullptr) {
		focusOnFirstChild();
	}
	else { 
		switch (direction) {
			case Direction::Up:
				focusOnPrevious(child, direction);
				break;
		
			case Direction::Left:
				focusOnPrevious(child, direction);
				break;

			case  Direction::Down:
				focusOnNext(child, direction);
				break;

			case Direction::Right:
				focusOnPrevious(child, direction);
				break;
		}
	}
}

std::shared_ptr<Widget> Container::findNextFocusableWidget(std::shared_ptr<Widget> widget){
	std::shared_ptr<Widget> next;

	for (auto child : mChildern) {
		if (child->isFocusTraversable()){
			auto childFocusIndex = child->focusIndex();
			
			// Find the first child idx that is smaller, then check for a closer match.
			
			if (next == nullptr){
				if (childFocusIndex > widget->focusIndex()) {
					next = child;
				}
			}
			else {
				if ((childFocusIndex > widget->focusIndex()) && 
				    (childFocusIndex < next->focusIndex())) {
					next = child;
				}
			}
		}
	}

	return next;
}

std::shared_ptr<Widget> Container::findPreviousFocusableWidget(std::shared_ptr<Widget> widget){
	std::shared_ptr<Widget> prev;

	for (auto child : mChildern) {
		if (child->isFocusTraversable()){
			auto childFocusIndex = child->focusIndex();

			// Find the first idx that is smaller, then check for a closer match.

			if (prev == nullptr){
				if (childFocusIndex < widget->focusIndex()) {
					prev = child;
				}
			}
			else {
				if ((childFocusIndex < widget->focusIndex()) && 
				    (childFocusIndex > prev->focusIndex())) {
					prev = child;
				}
			}

		}
	}

	return prev;
}

std::shared_ptr<Widget> Container::findFirstFocusableWidget(){
	// Start from a widget that is focus traversable	
	auto iter = std::find_if(mChildern.begin(), mChildern.end(),
				[](std::shared_ptr<Widget> &widget) -> bool { return widget->focusIndex() != -1; });

	std::shared_ptr<Widget> first = (iter == std::end(mChildern)) ? nullptr : *iter;	

	for (auto child : mChildern) {
		if (child->isFocusTraversable()){
			if (first->focusIndex() > child->focusIndex()){
				first = child;
			}
		}
	}

	return first;
}

std::shared_ptr<Widget> Container::findLastFocusableWidget(){
	auto iter = std::find_if(mChildern.begin(), mChildern.end(),
				[](std::shared_ptr<Widget> &widget) -> bool { return widget->focusIndex() != -1; });

	std::shared_ptr<Widget> last = ( iter == std::end(mChildern)) ? nullptr : *iter;	

	for (auto child : mChildern) {
		if (child->isFocusTraversable()){
			if (last->focusIndex() < child->focusIndex()){
				last = child;
			}
		}
	}

	return last;
}

void Container::focusOnNext(std::shared_ptr<Widget> widget, Direction direction){
	auto next = findNextFocusableWidget(widget);

	if (next != nullptr) {
		if (next->isContainer()){
			next->onChangeFocus(direction, nullptr);	
		}
		else {
		 	next->requestFocus();
		}
	}
	else if (parent() != nullptr) {
		parent()->onChangeFocus(direction, Widget::shared_from_this());
	}

}

void Container::focusOnPrevious(std::shared_ptr<Widget> widget, Direction direction){
	auto previous = findPreviousFocusableWidget(widget);

	if (previous != nullptr){
		if (previous->isContainer()) {
			previous->onChangeFocus(direction, nullptr);	
		}
		else {
			previous->requestFocus();
		}

	}
	else if (parent() != nullptr) {
		parent()->onChangeFocus(direction, Widget::shared_from_this());
	}
	
}

std::shared_ptr<Widget> Container::findWidgetWithFocusIndex(int idx){
	std::shared_ptr<Widget> widget;

	for (auto child : mChildern) {
		if (child->isFocusTraversable()){
			if (idx == child->focusIndex()){
				return child;
			}
		}
	}

	return widget;
}

void Container::assureAvailableFocusIndex(int index){
	auto widget = findWidgetWithFocusIndex(index);

	if (widget != nullptr){
		auto freeFocusIndex = findFirstAvailableFocusIndex();
		widget->setFocusIndex(freeFocusIndex);
	}
}

void Container::focusOnFirstChild() {
	auto widget = findFirstFocusableWidget();

	if (widget != nullptr) {
		if (widget->isContainer()){
			widget->onChangeFocus(Direction::Down, nullptr);	
		}
		else {
			widget->requestFocus();
		}
	} else if (parent() != nullptr) {
		parent()->onChangeFocus(Direction::Down, Widget::shared_from_this());
	}

}

void Container::focusOnLastChild(){
	auto widget = findLastFocusableWidget();

	if (widget != nullptr) {		
		if (widget->isContainer()){
			widget->onChangeFocus(Direction::Down, nullptr);	
		}
		else {
			widget->requestFocus();
		}
	} else if (parent() != nullptr) {
		parent()->onChangeFocus(Direction::Down, Widget::shared_from_this());
	}

}

void Container::focusToParent(Direction direction) {

	if (parent() != nullptr) {
		parent()->onChangeFocus(direction, Widget::shared_from_this());
	}
}
