
#pragma once
#include "Common.h"

struct Theme {

	static Color colorPrimary;
	static Color colorPrimaryDark;
	static Color colorPrimaryLight;

	static Color colorOnPrimary;

	static Color colorSecondary;
	static Color colorBackground;
};

