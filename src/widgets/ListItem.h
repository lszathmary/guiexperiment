#pragma once
#include "Common.h"
#include "Widget.h"
#include "Animation.h"

class ListItem : 
	public inheritable_shared_from_this<ListItem>, public Widget {
public:
    ListItem();

    std::string text() { return mText; }
	void setText(std::string text) { mText = text; }

    void draw(std::shared_ptr<Graphics> graphics);

private:
    std::string mText;
};
    