
#include <iostream>

#include "Widget.h"
#include "Screen.h"

Widget::Widget():
	mParent(nullptr),
	mFocusIndex(-1),
	mFocused(false),
	mBounds(0,0,100,35),
	mAbsoluteBounds(0,0,100,35) ,
	mInnerBounds(0,0,0,0) {
    mStyle.setBackgroundColor(Color(255, 0, 0, 255));

	initialize();
}

void Widget::initialize(){
	mAbsoluteBounds = computeAbsoluteBounds();
	mInnerBounds = computeInnerBounds();
	mContentBounds = computeContentBounds();
}

void Widget::update(){
	mAbsoluteBounds = computeAbsoluteBounds();
	mInnerBounds = computeInnerBounds();
	mContentBounds = computeContentBounds();
}

void Widget::draw(std::shared_ptr<Graphics> graphics) {
	graphics->setRenderColor(mStyle.backgroundColor());
	graphics->fillRectangle(mAbsoluteBounds);

	if (mStyle.border()){
		graphics->setRenderColor(mStyle.borderColor());
		graphics->drawRectangle(mAbsoluteBounds);
	}
}

Rectangle Widget::computeAbsoluteBounds() {
	Rectangle absoluteBounds = mBounds;

	if (mParent != nullptr) {
		absoluteBounds.x += mParent->absoluteBounds().x;
		absoluteBounds.y += mParent->absoluteBounds().y;
	}

	return absoluteBounds;
}

Rectangle Widget::computeInnerBounds() {
	Rectangle innerBounds = mAbsoluteBounds;

	if (mStyle.border()) {
		innerBounds.x += mStyle.borderWidth();
		innerBounds.y += mStyle.borderWidth();
		innerBounds.width  -= 2 * mStyle.borderWidth();
		innerBounds.height -= 2 * mStyle.borderWidth();
	}

	return innerBounds;
}

Rectangle Widget::computeContentBounds() {
	Rectangle contentBounds = mBounds;

	if (mParent != nullptr) {
		contentBounds.x = mStyle.borderWidth() + mStyle.padding();
		contentBounds.y = mStyle.borderWidth() + mStyle.padding();
		contentBounds.width -= 2 * mStyle.borderWidth() + 2 * mStyle.padding();
		contentBounds.height -= 2 * mStyle.borderWidth() + 2 * mStyle.padding();
	}

	return contentBounds;
}

bool Widget::contains(int x, int y) {
	Rectangle absoulteBounds = computeAbsoluteBounds();

	return !((x < mAbsoluteBounds.x) || (x >= mAbsoluteBounds.x + mAbsoluteBounds.width) ||
			 (y < mAbsoluteBounds.y) || (y >= mAbsoluteBounds.y + mAbsoluteBounds.height));
}

void Widget::onMouseDown(int x, int y){
	if (mActionListener != nullptr) {
		this->mActionListener();
	}
}

void Widget::onMouseUp(int x, int y){
	
}

void Widget::onMouseScroll(int x, int y, int delta){

}

void Widget::onMouseMove(int x, int y){
	
}

void Widget::onActionPerformed(){
	if (mActionListener != nullptr) {
		this->mActionListener();
	}
}

std::shared_ptr<Widget> Widget::findWidgetAt(int x, int y){
	return this->contains(x, y)	
		? shared_from_this()
		: nullptr;
}

void Widget::requestFocus() {
	mScreen->requestFocus(shared_from_this()); 
}

void Widget::onChangeFocus(Direction direction, std::shared_ptr<Widget> Widget){
	if (Widget == nullptr  && isFocusTraversable()) {
		requestFocus();
	}
	else {
		parent()->onChangeFocus(direction, shared_from_this());
	}
}

void Widget::onFocusChanged(bool focused){

}

void Widget::setScreen(std::shared_ptr<Screen> screen){ 
	this->mScreen = screen; 
}

void Widget::setActionListener(std::function<void()> listener) {
	this->mActionListener = listener;
}

void Widget::setFocusIndex(int focusIndex) { 
	if (mParent != nullptr) {
		mParent->assureAvailableFocusIndex(focusIndex);	
	}
	
	mFocusIndex = focusIndex;
}

void Widget::assureAvailableFocusIndex(int index) {
	
}