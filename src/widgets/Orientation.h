
/**
 * This type is used to signify an object's orientation.
 */
enum class Orientation {
	Vertical,
	Horizotnal
};