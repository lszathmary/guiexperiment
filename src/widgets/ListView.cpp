#include "ListView.h"
#include "Theme.h"
#include <algorithm>
#include "Theme.h"

ListView::ListView()
    : mScrollOffset(0), mItemHeight(55), mScrollBarWidth(5) {
    mStyle.setBorder(true);
    mStyle.setBackgroundColor(Theme::colorBackground);
    mStyle.setBorderColor(Theme::colorPrimaryLight);
    mStyle.setPadding(0);
}

void ListView::update() {
    Widget::update();
    
	mScroller.update();
    mScrollOffset = mScroller.getValue();

    // Compute indices of visible items
    mFirstVisibleIndex = (int) std::floor((mScrollOffset / (double) mItemHeight));
    mFirstVisibleIndex = std::max(0, mFirstVisibleIndex);

    mLastVisibleIndex = (int) std::ceil((mScrollOffset + height()) / (double)mItemHeight) - 1;
    mLastVisibleIndex = std::min((int)(mChildern.size() - 1), mLastVisibleIndex);

    // Determine space need for scrollbar
    int spacingOnRight = isScrollBarVisible() ? mScrollBarWidth : 0;

    // Update the bounds of the children
    for (int idx = mFirstVisibleIndex; idx <= mLastVisibleIndex; idx++) {
        auto child = mChildern[idx];
        auto childBounds = Rectangle();

        childBounds.x = contentBounds().x;
        childBounds.y = contentBounds().y + (idx * mItemHeight) - mScrollOffset;
        childBounds.width = contentBounds().width - spacingOnRight;
        childBounds.height = mItemHeight;

        child->setPosition(childBounds.x, childBounds.y);
        child->setSize(childBounds.width, childBounds.height);

        child->update();
    }
}

void ListView::draw(std::shared_ptr<Graphics> graphics) {
    Widget::draw(graphics);

    graphics->save();
    graphics->setClipRectangle(innerBounds());

    for (int idx = mFirstVisibleIndex; idx <= mLastVisibleIndex; idx++) {
        mChildern[idx]->draw(graphics);
    }

    if (isScrollBarVisible()) {
        drawScrollBar(graphics);
    }

    graphics->restore();
}

void ListView::onMouseScroll(int x, int y, int delta) {
	Container::onMouseScroll(x, y, delta);
		
    if (isScrollBarVisible()) {
        int maxOffset = (int) (mChildern.size() * mItemHeight - innerBounds().height);
        int scrollAmount = - delta * 2 * mItemHeight;

        mScroller.scrollBy(mScrollOffset, scrollAmount, 0, maxOffset);
    }
}

void ListView::drawScrollBar(std::shared_ptr<Graphics> graphics) {
	int contentHeight = (int) (mChildern.size() * mItemHeight);

	int thumbHeight = (contentBounds().height * contentBounds().height) / contentHeight;
	int thumbYPos = (mScrollOffset * contentBounds().height) / contentHeight;

    auto bounds = Rectangle();

    bounds.x = innerBounds().x + contentBounds().width - mScrollBarWidth;
    bounds.y = innerBounds().y + thumbYPos;

	bounds.width = mScrollBarWidth;
	bounds.height = thumbHeight;

    graphics->setRenderColor(Theme::colorPrimaryDark);
    graphics->fillRectangle(bounds);
}

bool ListView::isScrollBarVisible(){
    int contentHeight = (int) mChildern.size() * mItemHeight;

    return ((contentBounds().height - contentHeight) < 0);
}

void ListView::onChangeFocus(Direction direction, std::shared_ptr<Widget> child) {
    
    if (child == nullptr) {
        focusOnFirstChild();
    }
    else {
      switch (direction) {
            case Direction::Up:
                focusOnPrevious(child, direction);
                break;

            case Direction::Down:
                focusOnNext(child, direction);
                break;
        
            case Direction::Left:
                focusToParent(direction);
                break;

            case Direction::Right:
                focusToParent(direction);
                break;
        }		
    }

    
}

void ListView::scrollTo(std::shared_ptr<Widget> widget) {
    int childY = contentBounds().y + (widget->focusIndex() * mItemHeight);

    int scrollAmount = -mScrollOffset + childY;
    int maxOffset = (int) (mChildern.size() * mItemHeight - innerBounds().height);

    mScroller.scrollBy(mScrollOffset, scrollAmount, 0, maxOffset);
}
