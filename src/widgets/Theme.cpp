#include "Theme.h"

Color Theme::colorPrimary = Color(40, 55, 115, 255);
Color Theme::colorPrimaryDark = Color(38,52, 108,255);
Color Theme::colorPrimaryLight = Color(66, 90, 187, 255);
Color Theme::colorOnPrimary = Color(167, 203, 239, 255);

Color Theme::colorSecondary = Color(230, 50, 50, 255);
Color Theme::colorBackground =  Color(52,71, 150,255);
