#pragma once
#include "Common.h"
#include "Timer.h"
#include "Widget.h"

class Animation
{
public:
	Animation();
	
	void set_rectangle(Rectangle rectangle);
	Rectangle get_rectangle() { return scaled_rectangle; }
	void update();
	void play();
private:
	Rectangle rectange;
	Rectangle scaled_rectangle;
	bool playing;

	double scale;
	double delta;
	uint32_t start;
	uint32_t duration;
};

class ScaleAnimation
{
public:
	ScaleAnimation();

	void set_vertical_scale(double scale) { this->vertical_scale = scale; }
	void set_horizontal_scale(double scale) { this->horizontal_scale = scale;  }

	void set_new_height(int new_height) { this->new_height = new_height; }
	void set_new_width(int new_width) { this->new_width = new_width; }

	
	void set_widget(std::shared_ptr<Widget> widget) { this->widget = widget; }
	std::shared_ptr<Widget> get_widget() { return this->widget; }
	   
	void play();
	void update();
	void end();
	void pause();

	bool is_playing() { return playing; }
	

private:
	void scale_widget(uint32_t elapsed);
	void zoom_widget(uint32_t elalpsed);

	bool playing;

	int start_width;
	int start_height;
	

	int new_width;
	int new_height;

	uint32_t start_time;
	uint32_t duration;

	Rectangle start_bound;

	double horizontal_scale;
	double vertical_scale;

	std::shared_ptr<Widget> widget;
	Timer timer;
};