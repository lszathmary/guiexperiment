#include "ListItem.h"
#include "Theme.h"

ListItem::ListItem() {
	mText = "button";
	mStyle.setBorderWidth(3);
	mStyle.setBackgroundColor(Color(255, 255, 255,0));
	mStyle.setBorderColor(Color(255, 255, 255, 0));
	mStyle.setHighlightColor(Theme::colorPrimaryDark);
}

void ListItem::draw(std::shared_ptr<Graphics> graphics) {
	Widget::draw(graphics);

	if (mFocused) {
		graphics->setRenderColor(mStyle.highlightColor());
		graphics->fillRectangle(innerBounds());
	}

	auto textBounds = innerBounds();
	textBounds.x += 10;

	graphics->setRenderColor(mStyle.foregroundColor());
	graphics->drawText(textBounds, mText, Alignment::Left);
}
