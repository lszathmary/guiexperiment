#include "GridView.h"
#include <algorithm>

GridView::GridView():
	offset_x(0),
	offset_y(0),
	first_visible_idx(0),
	last_visible_idx(0),
	spacing(20),
	cell_width(230),
	cell_height(300),
	visible_column_count(0),
	visible_row_count(0) {

	
	//this->style.background_color = { 135, 206, 250, 0 };
	
	//this->style.background_color = { 70,130,180, 0 };
	this->mStyle.setBackgroundColor(Color(70, 71, 105, 0));
	
}

void GridView::update() {
	Widget::update();

	scroller.update();

	// if scroll has update we could notify the parent


	offset_y = scroller.getValue();
	// center elements horizontally
	offset_x = compute_offset_x();

	visible_column_count = width() / get_cell_width_with_spacing();
	visible_row_count = height() / get_cell_height_with_spacing();

	first_visible_idx = floor(offset_y / (double) get_cell_height_with_spacing()) * visible_column_count;
	last_visible_idx = ceil((double)(offset_y + height()) / (double)get_cell_height_with_spacing()) * visible_column_count - 1;
	
	// Perform bound check
	first_visible_idx = std::max(0, first_visible_idx);
	last_visible_idx = std::min((int)(mChildern.size() - 1), last_visible_idx);

	for (int idx = first_visible_idx; idx < (last_visible_idx + 1); idx++) {
		update_child(idx);
	}
}

void GridView::update_child(int idx) {
	auto item = mChildern[idx];
	
	Rectangle outer_bounds;

	outer_bounds.y = -offset_y + spacing;
	outer_bounds.x =  offset_x + spacing;
	
	outer_bounds.x += (idx % visible_column_count) * get_cell_width_with_spacing();
	outer_bounds.y += (idx / visible_column_count) * get_cell_height_with_spacing();

	outer_bounds.width = cell_width;
	outer_bounds.height = cell_height;
		
	Rectangle inner_bounds;

	// center item in the grid;
	int free_width = (cell_width - item->absoluteBounds().width);
	int free_height = (cell_height - item->absoluteBounds().height);

	inner_bounds.x = outer_bounds.x + (free_width/2);
	inner_bounds.y = outer_bounds.y + (free_height/2);

	// limit size of the widget
	inner_bounds.width = std::min(item->bounds().width, cell_width);
	inner_bounds.height = std::min(item->bounds().height, cell_height);
	
	item->setPosition(inner_bounds.x, inner_bounds.y);
	item->setSize(inner_bounds.width, inner_bounds.height);
	item->update();
}

void GridView::draw(std::shared_ptr<Graphics> graphics) {
	//Widget::draw(graphics);
	//graphics->fill_gradient_rect(this->style.background_color, absolute_bounds, *gradient_background);
	//graphics->draw_image(absolute_bounds, *gradient_background);
	if (gradient_background != nullptr) {
		graphics->drawTexture(mAbsoluteBounds, *gradient_background);
	}

	graphics->setClipRectangle(this->mAbsoluteBounds);

	Color color = Color( 0, 200, 0,0);
	//graphics->draw_rectangle(color, absolute_bounds);
		
	for (int idx = first_visible_idx; idx < (last_visible_idx + 1); idx++) {
		mChildern[idx]->draw(graphics);
	}

	Color color2 = Color(100, 200,100, 0);
	//graphics->draw_rectangle(color2, absolute_bounds);
}

void GridView::onMouseScroll(int x, int y, int delta) {
	Container::onMouseScroll(x, y, delta);
	auto totat_row_count = ceil((double)mChildern.size() / (double)visible_column_count);
	auto max_scroll_value = (int)((totat_row_count * get_cell_height_with_spacing()) - height());
	
	int amount = -delta * cell_height;
	scroller.scrollBy(offset_y, amount, 0, max_scroll_value);
}

void GridView::onMouseMove(int x, int y) {

}


int GridView::get_cell_width_with_spacing() {
	return cell_width + (2 * spacing);
}

int GridView::get_cell_height_with_spacing() {
	return cell_height + (2 * spacing);
}

int GridView::compute_offset_x() {
	int content_width = visible_column_count * get_cell_width_with_spacing();
	int remaining_width = width() - content_width;

	return std::max(0, remaining_width / 2);
}

void GridView::onChangeFocus(Direction direction, std::shared_ptr<Widget> widget) {
	if (widget == nullptr)  {
		if (this->mChildern.size() != 0) {
			//scroll to and request focuse
			mChildern[0]->requestFocus();
		}
	}
	else {
		auto it = std::find(mChildern.begin(), mChildern.end(), widget);

		if (direction == Direction::Right) {
			if ((it != mChildern.end()) && (next(it) != mChildern.end())) {
				auto next_widget = std::next(it, 1);
				int index = std::distance(mChildern.begin(), next_widget);
				//cout << index << endl;
				ensure_visible(index);
				(*next_widget)->requestFocus();
			}
		}
		if (direction == Direction::Left){
			if ((it != mChildern.begin()) && (next(it) != mChildern.begin())) {
				auto next_widget = std::prev(it, 1);
				int index = std::distance(mChildern.begin(), next_widget);
				//cout << index << endl;
				ensure_visible(index);
				(*next_widget)->requestFocus();
			}
		}

		if (direction ==  Direction::Down) {
				int index = std::distance(mChildern.begin(), it);
				int next_index = index + visible_column_count;

				if (next_index < mChildern.size()) {
					ensure_visible(next_index);
					mChildern[next_index]->requestFocus();
				}
		}
		

		if (direction ==  Direction::Up) {
			int index = std::distance(mChildern.begin(), it);
			int next_index = index - visible_column_count;

			if (next_index >= 0) {
				ensure_visible(next_index);
				mChildern[next_index]->requestFocus();
			}
		}
	

		//auto it = std::find(childern.begin(), childern.end(), widget);
		// wtf is last??
		/*
		if ((it != childern.end()) && (next(it) != childern.end())){ 
			auto next_widget = next(it, 1);
			int index = distance(childern.begin(), next_widget);
			cout << index << endl;
			ensure_visible(index);
			(*next_widget)->request_focus();
		}
		*/
		
	}
	/*
	auto it = std::find(childern.begin(), childern.end(), Widget);
	
	auto next_widget = next(it, 1);
	(*next_widget)->request_focus();
	//this->get_parent()->focus_change(direction, shared_from_this());
	*/
}

Rectangle GridView::compute_cell_bounds(int idx) {
	Rectangle outer_bounds;

	outer_bounds.y = -offset_y + spacing;
	outer_bounds.x = offset_x + spacing;

	outer_bounds.x += (idx % visible_column_count) * get_cell_width_with_spacing();
	outer_bounds.y += (idx / visible_column_count) * get_cell_height_with_spacing();

	outer_bounds.width = cell_width;
	outer_bounds.height = cell_height;

	return outer_bounds;
}

void GridView::ensure_visible(int idx) {

	Rectangle cell_bounds = compute_cell_bounds(idx);


	if (mAbsoluteBounds.contains(cell_bounds)) {
	
			return;
		
	}

	auto delta = mAbsoluteBounds.y - cell_bounds.y;
	int x = 0;


	auto totat_row_count = ceil((double)mChildern.size() / (double)visible_column_count);
	auto max_scroll_value = (int)((totat_row_count * get_cell_height_with_spacing()) - height());

	//int amount = -delta * cell_height;
	scroller.scrollBy(offset_y, -delta, 0, max_scroll_value);
}