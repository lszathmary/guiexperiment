#include "VBoxContainer.h"

#include <algorithm>

VBoxContainer::VBoxContainer():
	mSpacing(10) {
}

void VBoxContainer::update() {
	Widget::update();

	auto offset = mSpacing;
	
	int remaningHeight = computeRemainingHeight();
	int widgetCount = widgetCountWithVerticalGrowth();
	
	int autoHeight = remaningHeight / (std::max(1, widgetCount));
	int autoWidth  = width() - (2 * mSpacing);

	for (auto child : mChildern) {
		if (mVerticalConstraints[child]) {
			child->setPosition(mSpacing, offset);
			child->setHeight(autoHeight);
		}
		else {
			child->setPosition(mSpacing, offset);
		}

		offset += mSpacing + child->height();

		if (mHorizontalConstraints[child]) {
			child->setWidth(autoWidth);
		}

		child->update();
	}
}

void VBoxContainer::draw(std::shared_ptr<Graphics> graphics) {

	graphics->save();
	graphics->setClipRectangle(mAbsoluteBounds);

	Container::draw(graphics);

	graphics->restore();
}

void VBoxContainer::removeChild(std::shared_ptr<Widget> child){
	Container::removeChild(child);
	
	if (mVerticalConstraints.find(child) != mVerticalConstraints.end()){
		mVerticalConstraints.erase(child);
	}
	
	if (mHorizontalConstraints.find(child) != mHorizontalConstraints.end()){
		mHorizontalConstraints.erase(child);
	}
}

bool VBoxContainer::verticalGrowth(std::shared_ptr<Widget> widget) {
	return mVerticalConstraints.at(widget);
}

void VBoxContainer::setVerticalGrowth(std::shared_ptr<Widget> widget, bool grow) {
	if (std::find(mChildern.begin(), mChildern.end(), widget) == mChildern.end()) {
		throw std::exception("The widget is not a child of this container");
	}
	
	mVerticalConstraints.insert({ widget, grow });
}

bool VBoxContainer::horizontalGrowth(std::shared_ptr<Widget> widget) {
	return mHorizontalConstraints.at(widget);
}

void VBoxContainer::setHorizontalGrowth(std::shared_ptr<Widget> widget, bool grow) {
	if (std::find(mChildern.begin(), mChildern.end(), widget) == mChildern.end()) {
		throw std::exception("The widget is not a child of this container");
	}

	mHorizontalConstraints.insert({ widget, grow });
}

int VBoxContainer::computeRemainingHeight() {
	int remainingHeight = height() - mSpacing;

	for (auto child : mChildern) {
		if (!mVerticalConstraints[child]) {
			remainingHeight -= child->height();
		}

		remainingHeight -= mSpacing;
	}

	return std::max(25, remainingHeight);
}

int VBoxContainer::computeRemainingWidth() {
	int remainingWidth = width() - mSpacing;

	for (auto child : mChildern) {
		if (!mHorizontalConstraints[child]) {
			remainingWidth -= (child->width());
		}

		remainingWidth -= mSpacing;
	}

	return std::max(25, remainingWidth);
}

int VBoxContainer::widgetCountWithHorizontalGrowth() {
	int count = 0;

	for (auto child : mChildern) {
		if (mHorizontalConstraints[child]) {
			count++;
		}
	}
	
	return count;
}

int VBoxContainer::widgetCountWithVerticalGrowth() {
	int count = 0;

	for (auto child : mChildern) {
		if (mVerticalConstraints[child]) {
			count++;
		}
	}

	return count;
}

void VBoxContainer::onChangeFocus(Direction direction, std::shared_ptr<Widget> child) {
	if (mChildern.empty()){
		return;
	}

	if (child == nullptr) {
		focusOnFirstChild();
	}
	else {
        switch (direction) {
            case Direction::Up:
                focusOnPrevious(child, direction);
                break;

            case Direction::Down:
                focusOnNext(child, direction);
                break;
        
            case Direction::Left:
                focusToParent(direction);
                break;

            case Direction::Right:
                focusToParent(direction);
                break;
        }		
    }
		
}



