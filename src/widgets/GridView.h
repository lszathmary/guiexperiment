#pragma once
#include "Container.h"
#include "GridItem.h"
#include "Scroller.h"


// Todo:
// - set min widht
// - dont scroll if there are no elements
class GridView: public inheritable_shared_from_this <GridView>, public Container {

public:
	// on scroll focus can change
	// vw could handle focus animation here
	GridView();

	virtual void update() override;
	virtual void draw(std::shared_ptr<Graphics> graphics) override;

	virtual void onMouseScroll(int x, int y, int delta) override;
	virtual void onMouseMove(int x, int y) override;
	virtual void onChangeFocus(Direction direction, std::shared_ptr<Widget> Widget) override;

	void set_spacing(int spacing) { this->spacing = spacing; }
	int get_spacing() { return this->spacing; }

	// set cell_widh
	// set cell_height

	// set resize_children
	// align children(center, vertically horizontally)
	// limit size of the childer)

	Rectangle compute_cell_bounds(int idx);

	void set_gradient_bg(std::shared_ptr<Texture> bg) { this->gradient_background = bg; }
	std::shared_ptr<Texture>  get_gradient_bg(std::shared_ptr<Image> bg) { return this->gradient_background; }

private:

	void ensure_visible(int idx);
	void update_child(int idx);

	int get_cell_width_with_spacing();
	int get_cell_height_with_spacing();

	int compute_offset_x();

private:
	std::shared_ptr<Texture> gradient_background;
	int offset_y;
	int offset_x;
	// spacing between the widget
	int spacing;

	int first_visible_idx;
	int last_visible_idx;

	int visible_row_count;
	int visible_column_count;
	// may be this should be max size
	int cell_height;
	int cell_width;

	Scroller scroller;

};


