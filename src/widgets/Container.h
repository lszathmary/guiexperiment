#pragma once

#include <vector>
#include "Widget.h"


// Fix assign existing focus index
// How to decide scroll order
// Where should focus go when pressing right/left
/**
 * Base class for widgets that can have children.
 */
class Container : 
	public inheritable_shared_from_this <Container>, public Widget
{
	
public:
	// Construct a new container.
	Container();
			
	virtual void setScreen(std::shared_ptr<Screen> screen) override;

	// Add a child widget to the container
	virtual void addChild(std::shared_ptr<Widget> child);
	// Remove the child of the widget
	virtual void removeChild(std::shared_ptr<Widget> child);
	/// Retrieves the child at the specific position
    std::shared_ptr<Widget> childAt(int index) { return mChildern[index]; }

	// Return the background texture of the container.
	std::shared_ptr<Texture> backgroundTexture(std::shared_ptr<Texture> texture) { return mBackgroundTexture; }
	// Set the background texture of the widget
	void setBackgroundTexture(std::shared_ptr<Texture> texture) { this->mBackgroundTexture = texture; }
	
	virtual bool isFocusTraversable() override { return true; }

	virtual bool isContainer() { return true; }
		
	virtual void assureAvailableFocusIndex(int index) override;
	
	virtual std::shared_ptr<Widget> findWidgetAt(int x, int y) override;

	virtual void onChangeFocus(Direction direction, std::shared_ptr<Widget> Widget) override;

	virtual void onMouseScroll(int x, int y, int delta) override;

	virtual void update() override;
	
	virtual void draw(std::shared_ptr<Graphics> graphics) override;

protected:

	// Move the focus to the widget located before the specified widget
	void focusOnNext(std::shared_ptr<Widget> widget, Direction direction);
	// Move the focus to the widget located after the specified widget
	void focusOnPrevious(std::shared_ptr<Widget> widget, Direction direction);

	/// Returns the previous focusable widget or nullptr if there is no previous widget
	std::shared_ptr<Widget> findNextFocusableWidget(std::shared_ptr<Widget> widget);
	/// Returns the next focusable widget or nullptr if there is no next widget
	std::shared_ptr<Widget> findPreviousFocusableWidget(std::shared_ptr<Widget> widget);

	/// Returns first focusable widget or nullptr if there is none
	std::shared_ptr<Widget> findFirstFocusableWidget();
	/// Returns last focusable widget or nullptr if there is none
	std::shared_ptr<Widget> findLastFocusableWidget();

	/// Apply the foucus to the first child of the container.
	void focusOnFirstChild();
	/// Apply the focus to the last child of the container.
	void focusOnLastChild();

	// Find the smallest free focus index that can be used.
	int findFirstAvailableFocusIndex();

	// Find the widget that has the specified focus index.
	std::shared_ptr<Widget> findWidgetWithFocusIndex(int index);

	// Pass the focus to the parent container
	void focusToParent(Direction direction);

protected:
	// The children of the widget
	std::vector<std::shared_ptr<Widget>> mChildern;

	// The background texture of the widget
	std::shared_ptr<Texture> mBackgroundTexture;

};

