#pragma once

#include "Widget.h"
#include <string>
using namespace std;

/**
 * TODO:
 *  Add multi line rendering
 *  Can label be transparent
 */
class Label :
	public inheritable_shared_from_this <Container>, public Widget
{
public:
	Label();

	string text() { return mText; }
	void setText(string text) { mText = text; }
	
	virtual bool isFocusTraversable() override { return false; }
	
	virtual void draw(shared_ptr<Graphics> graphics) override;

protected:
	string	mText;
};

