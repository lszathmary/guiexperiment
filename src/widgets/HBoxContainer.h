#pragma once

#include <vector>
#include <map>
#include "Widget.h"
#include "Container.h"
#include "Orientation.h"
#include "Common.h"

/**
 * A Container that lays out its children horizontally.
 */
class HBoxContainer :
	public inheritable_shared_from_this <HBoxContainer>, public Container
{
public:
	HBoxContainer();
	
	virtual void removeChild(std::shared_ptr<Widget> child) override;

	// Return spacing between the child widgets
	int spacing() { return mSpacing; }
	// Set the spacing between child widgets
	void setSpacing(int padding) { mSpacing = padding;  }

	// Returns true if widget will be resized vertically
	bool verticalGrowth(std::shared_ptr<Widget> widget);
	// Set the whether the widgets should resize vertically
	void setVerticalGrowth(std::shared_ptr<Widget> child, bool grow);

	// Returns true if widget will be resized horizontally
	bool horizontalGrowth(std::shared_ptr<Widget> widget);
	// Set the whether the widgets should resize horizontally
	void setHorizontalGrowth(std::shared_ptr<Widget> child, bool grow);

	virtual void onChangeFocus(Direction direction, std::shared_ptr<Widget> Widget) override;

	virtual void update() override;
	
	virtual void draw(std::shared_ptr<Graphics> grapihcs) override;

protected:
	
	// Compute the width remanining for widgets that have to be autosized
	int computeRemainingWidth();
	// Compute the height remanining for widgets that have to be autosized
	int computeRemainingHeight();

	// Compute the number of widgets have to be resized horizontally
	int widgetCountWithHorizontalGrowth();
	// Compute the number of widgets have to be resized vertically
	int widgetCountWithVerticalGrowth();
	
private:

	// The space between child components.
	int mSpacing;
	// Store horizontal growth information about widget.
	std::map<std::weak_ptr<Widget>, bool, std::owner_less<>> mHorizontalConstraints;
	// Store vertical growth information about widget.
	std::map<std::weak_ptr<Widget>, bool, std::owner_less<>> mVerticalConstraints;
};

