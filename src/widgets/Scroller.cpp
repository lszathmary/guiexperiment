#include "Scroller.h"
#include "Common.h"
#include <algorithm>
#include "Utils.h"

Scroller::Scroller():
	is_scrolling(false),
	scroll_amount(0),
	scroll_value(0),
	start_time(0),
	duration(200) {

}

void Scroller::scrollBy(int from, int delta, int min, int max) {
	this->start_time = getTimeInMilliSeconds();
	this->scroll_start = from;
	this->scroll_amount = delta;
	this->min_scroll = min;
	this->max_scroll = max;
	this->is_scrolling = true;
}

void Scroller::update() {
	if (is_scrolling == false)
		return;
	
	uint32_t elapsed = getTimeInMilliSeconds() - start_time;
	elapsed = std::min(elapsed, duration);
	this->scroll_value = scroll_start + (((double)elapsed / (double)duration) * (double)scroll_amount);
	this->scroll_value = std::max(scroll_value, min_scroll);
	this->scroll_value = std::min(scroll_value, max_scroll);
	if (elapsed == duration) {
		is_scrolling = false;
	}

}