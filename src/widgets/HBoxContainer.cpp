#include "HBoxContainer.h"

#include <algorithm>

HBoxContainer::HBoxContainer():
	mSpacing(10) {
}

void HBoxContainer::update() {
	Widget::update();

	auto offset = mSpacing;

	int remaningWidth = computeRemainingWidth();
	int widgetCount = widgetCountWithHorizontalGrowth();
	
	int autoWidth  = remaningWidth / (std::max(1, widgetCount));
	int autoHeight = height() - (2 * mSpacing);

	for (auto child : mChildern) {
		if (mHorizontalConstraints[child]) {
			
			child->setPosition(offset, mSpacing);
			child->setWidth(autoWidth);
		}
		else {

			child->setPosition(offset, mSpacing);
		}

		offset += mSpacing + child->width();
		
		if (mVerticalConstraints[child]) {
			child->setHeight(autoHeight);
		}
		
		child->update();
	}
}

void HBoxContainer::draw(std::shared_ptr<Graphics> graphics) {

	graphics->save();
	graphics->setClipRectangle(mAbsoluteBounds);

	Container::draw(graphics);

	graphics->restore();
}

void HBoxContainer::removeChild(std::shared_ptr<Widget> child){
	Container::removeChild(child);
	
	if (mVerticalConstraints.find(child) != mVerticalConstraints.end()){
		mVerticalConstraints.erase(child);
	}
	
	if (mHorizontalConstraints.find(child) != mHorizontalConstraints.end()){
		mHorizontalConstraints.erase(child);
	}
}

bool HBoxContainer::verticalGrowth(std::shared_ptr<Widget> widget) {
	return mVerticalConstraints.at(widget);
}

void HBoxContainer::setVerticalGrowth(std::shared_ptr<Widget> widget, bool grow) {
	if (std::find(mChildern.begin(), mChildern.end(), widget) == mChildern.end()) {
		throw std::exception("The widget is not a child of this container");
	}
	
	mVerticalConstraints.insert({ widget, grow });
}

bool HBoxContainer::horizontalGrowth(std::shared_ptr<Widget> widget) {
	return mHorizontalConstraints.at(widget);
}

void HBoxContainer::setHorizontalGrowth(std::shared_ptr<Widget> widget, bool grow) {
	if (std::find(mChildern.begin(), mChildern.end(), widget) == mChildern.end()) {
		throw std::exception("The widget is not a child of this container");
	}

	mHorizontalConstraints.insert({ widget, grow });
}

int HBoxContainer::computeRemainingHeight() {
	int remainingHeight = height() - mSpacing;

	for (auto child : mChildern) {
		if (!mVerticalConstraints[child]) {
			remainingHeight -= child->height();
		}

		remainingHeight -= mSpacing;
	}

	return std::max(25, remainingHeight);
}

int HBoxContainer::computeRemainingWidth() {
	int remainingWidth = width() - mSpacing;

	for (auto child : mChildern) {
		if (!mHorizontalConstraints[child]) {
			remainingWidth -= (child->width());
		}

		remainingWidth -= mSpacing;
	}

	return std::max(25, remainingWidth);
}

int HBoxContainer::widgetCountWithHorizontalGrowth() {
	int count = 0;

	for (auto child : mChildern) {
		if (mHorizontalConstraints[child]) {
			count++;
		}
	}
	
	return count;
}

int HBoxContainer::widgetCountWithVerticalGrowth() {
	int count = 0;

	for (auto child : mChildern) {
		if (mVerticalConstraints[child]) {
			count++;
		}
	}

	return count;
}

void HBoxContainer::onChangeFocus(Direction direction, std::shared_ptr<Widget> child) {
	if (mChildern.empty()){
		return;
	}

	if (child == nullptr) {
		focusOnFirstChild();
	}
	else {
		switch (direction) {
            case Direction::Left:
                focusOnPrevious(child, direction);
                break;

            case Direction::Right:
                focusOnNext(child, direction);
                break;

            case Direction::Down:
                focusToParent(direction);
                break;

            case Direction::Up:
                focusToParent(direction);
                break;
	    }
	}
}


