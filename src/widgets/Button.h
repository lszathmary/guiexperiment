#pragma once

#include "Label.h"
#include <string>

using namespace std;

class Button
	: public inheritable_shared_from_this<Button>, public Label
{
public:
	Button();
		
	std::shared_ptr<Texture> texture(std::shared_ptr<Texture> texture) { return mTexture; }
	void setTexture(std::shared_ptr<Texture> texture) { mTexture = texture; }
	
	virtual bool isFocusTraversable() override { return true; }
			
	virtual void draw(std::shared_ptr<Graphics> graphics) override;

private:
	std::shared_ptr<Texture> mTexture;

};

