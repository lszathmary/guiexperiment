#include "Application.h"
#include "Graphics.h"


Application::Application(){
}

Application::~Application(){
}

bool Application::init(){
	this->mouse_state.active = true;

	SDL_Init(SDL_INIT_EVERYTHING);
	//this->window = SDL_CreateWindow("title", 50, 50, 1024, 768, SDL_WINDOW_SHOWN);
	//this->window = SDL_CreateWindow("title", 50, 50, 1152, 864, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);

	
	this->window = SDL_CreateWindow("Sample", 50, 50, 1280, 800, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	//this->window = SDL_CreateWindow("title", 50, 50, 1280, 800, SDL_WINDOW_FULLSCREEN | SDL_WINDOW_RESIZABLE);
	//this->window = SDL_CreateWindow("title", 0, 0, 1680, 1050, SDL_WINDOW_FULLSCREEN | SDL_WINDOW_RESIZABLE);
	this->graphics = std::shared_ptr<Graphics>(new Graphics(window));
	this->screen = std::shared_ptr<Screen>(new Screen(graphics));

	this->image = std::shared_ptr<Image>(new Image());
	this->image2 = std::shared_ptr<Image>(new Image());
	//image->load_from_file("shadow.png");
		//image->load_from_file("soul.jpg");
	//image->load_from_file("transparent.png");
	image->loadFromFile("transparent.png");
	image2->loadFromFile("god.jpg");
	//image->load_from_file("shadow.png", 180, 255,20);
	//image->generate(180, 255, 10);
	//image->load_from_file("001.jpg", 180,255);
	//image->load_from_file("soul.jpg", 180, 255);

	SDL_Joystick* joy;
	if (SDL_NumJoysticks() > 0) {
		// Open joystick
		joy = SDL_JoystickOpen(0);
	}

	return true;
}

void Application::run() {
	
	auto running = true;
	while (running) {
		SDL_Event event;

		if (SDL_PollEvent(&event)) {
			switch (event.type) {

				case SDL_WINDOWEVENT:
					if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
						this->on_resize();
					}
					break;

				case SDL_QUIT:
					running = false;
					break;

				case SDL_MOUSEMOTION:
						this->move_mouse(event.motion.x, event.motion.y);
					break;
					
				case SDL_MOUSEBUTTONDOWN:
						this->mouse_down(event.motion.x, event.motion.y);
					break;

				case SDL_MOUSEBUTTONUP:
						this->mouse_up(event.motion.x, event.motion.y);
					break;

				case SDL_MOUSEWHEEL:
						this->mouse_scroll(event.wheel.y);
					break;

				case SDL_JOYHATMOTION:
				{
					this->assure_gamepad_input_mode();
					if (event.jhat.value == SDL_HAT_LEFT) {
						this->screen->onMoveBack();
					}
					if (event.jhat.value == SDL_HAT_RIGHT) {
						this->screen->onMoveNext();
					}

					if (event.jhat.value == SDL_HAT_UP) {
						this->screen->omNoveUp();
					}
					if (event.jhat.value == SDL_HAT_DOWN) {
						this->screen->omMoveDown();
					}
				}
					break;

				case SDL_JOYBUTTONDOWN:
					this->assure_gamepad_input_mode();
					if (event.jbutton.button == SDL_CONTROLLER_BUTTON_A) { // start on xbox 360 controller
						this->screen->onActionPeformed();
					}

					if (event.jbutton.button == SDL_CONTROLLER_BUTTON_B) { // start on xbox 360 controller
						this->screen->onBackPressed();
					}
					break;
					
				case SDL_KEYDOWN:

					switch (event.key.keysym.sym)
					{
					case SDLK_RIGHT:
						this->screen->onMoveNext();
						break;

					case SDLK_LEFT:
						this->screen->onMoveBack();
						break;
					
					case SDLK_ESCAPE:
						this->screen->onBackPressed();
						break;
					}
					break;
			}
		}

		// right now the layout can change any time
		// for example while scrollin
		// we need to assure the correct is selected
		//this->screen->assure_focused(this->mouse_state.x, this->mouse_state.y);
		
		screen->update();
		graphics->setRenderColor(Color(0, 0, 0, 0));
		graphics->clear();

		//graphics->setRenderColor(Color(255, 0, 0, 255));
		//graphics->drawRectangle(Rectangle(50, 50, 100, 100));

		//graphics->setRenderColor(Color(0, 255, 0, 255));
		//graphics->drawRectangle(Rectangle(51, 51, 98, 98));
		

		screen->draw(graphics);
		graphics->present();

	};
}

void Application::close() {
	SDL_Quit();
}

void Application::set_screen(shared_ptr<Screen> screen) {
	this->screen = screen;
	this->on_resize();
}

shared_ptr<Screen> Application::get_screen() {
	return this->screen;
}

shared_ptr<Texture>
Application::load_texture(string path) {
	shared_ptr<Texture> texture(new Texture(graphics));
	texture->loadFromFile(path);

	return texture;
}

shared_ptr<Texture>
Application::load_texture(string path, int borderSize) {
	shared_ptr<Texture> texture(new Texture(graphics));
	texture->loadFromFile(path, borderSize);

	return texture;
}


void 
Application::move_mouse(int x, int y){
	this->assure_mouse_input_mode();
	
	this->mouse_state.x = x;
	this->mouse_state.y = y;

	if (this->screen != nullptr) {
		this->screen->onMouseMove(this->mouse_state.x, this->mouse_state.y);
	}
}

void
Application::mouse_down(int x, int y){
	this->assure_mouse_input_mode();

	this->mouse_state.mouse_down = true;

	if (this->screen != nullptr) {
		this->screen->onMouseDown(this->mouse_state.x, this->mouse_state.y);
	}
}

void
Application::mouse_up(int x, int y){
	this->assure_mouse_input_mode();

	this->mouse_state.mouse_down = false;

	if (this->screen != nullptr) {
		this->screen->onMouseUp(this->mouse_state.x, this->mouse_state.y);
	}
}

void
Application::mouse_scroll(int delta){
	this->assure_mouse_input_mode();

	if (this->screen != nullptr) {
		screen->onMouseScroll(this->mouse_state.x, this->mouse_state.y, delta);
	}
}

void
Application::assure_gamepad_input_mode(){
	if (this->mouse_state.active == false){
		return;
	}

	this->mouse_state.active = false;
	SDL_ShowCursor(SDL_DISABLE);
}

void
Application::assure_mouse_input_mode(){
	if (this->mouse_state.active == true){
		return;
	}

	this->mouse_state.active = true;
	SDL_ShowCursor(SDL_ENABLE);
}

void
Application::on_resize(){
	if (screen != nullptr){
		Size size = graphics->ouputSize();
		screen->onResize(size.width, size.height);
	}
}