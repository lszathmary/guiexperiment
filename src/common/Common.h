#pragma once

#include <SDL.h>
#include <chrono>
#include <iostream>

enum class Direction
{
	First = 0,
	Last,
	Left, 
	Right,
	Up,
	Down
};

enum class Alignment
{
	Left = 0,
	Center,
	Right
};

/**
 * A structure that represent a point.
 */
struct Point {
	int x;
	int y;
};

/**
 * The structure defines the size of a widget.
 */
struct Size {
	int width;
	int height;

	Size();
	Size(int w, int h);
};

/**
 * A structure that represents a color. 
 */
struct Color {
	Uint8 red;
	Uint8 green;
	Uint8 blue;
	Uint8 alpha;

	Color();
	Color(int r, int g, int b, int a);
	Color(int r, int g, int b);
	Color(Color &other);

	operator SDL_Color() {
		SDL_Color color = { red, green, blue, alpha };
		
		return color;
	}
};

/**
 * A structure that represents a color. 
 */
struct Rectangle {
	int x;
	int y;
	int width;
	int height;

	Rectangle();
	Rectangle(int x, int y, int width, int height);
	Rectangle(Rectangle &other);

	operator SDL_Rect() {
		SDL_Rect rect = { x, y, width, height };

		return rect;	
	}

	inline bool contains(Rectangle other) {
		if ((other.x + other.width) < (x + width) 
			&& (other.x) > (x)
			&& (other.y) > (y) 
			&& (other.y + other.height) < (y + height))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	inline int bottom(){
		return y + height;
	}

	inline int right() {
		return x + width;
	}
};

