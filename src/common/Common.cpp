#include "Common.h"

/*
uint32_t get_current_time_in_ms() {
	auto  now = std::chrono::system_clock::now().time_since_epoch();
	auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(now).count();
	
	return static_cast<uint32_t>(millis);
}
*/

Size::Size(){

}

Size::Size(int w, int h):width(w),height(h){

}

Color::Color() {
	Color(0, 0, 0, 0);
}

Color::Color(int r, int g, int b, int a):
	red(r),green(g),blue(b),alpha(a) {

}

Color::Color(int r, int g, int b):
	red(r),green(g),blue(b) {

}

Color::Color(Color &other):
	red(other.red),green(other.green),blue(other.blue),alpha(other.alpha) {

}

Rectangle::Rectangle() {

}

Rectangle::Rectangle(int x, int y, int width, int height):
	x(x),y(y),width(width),height(height) {

}

Rectangle::Rectangle(Rectangle &other):
	x(other.x),y(other.y),width(other.width),height(other.height) {

}
