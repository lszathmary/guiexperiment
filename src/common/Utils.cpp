#include "Utils.h"
#include <chrono>

uint32_t getTimeInMilliSeconds(){
    auto  now = std::chrono::system_clock::now().time_since_epoch();
	auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(now).count();
	
	return static_cast<uint32_t>(millis);
}