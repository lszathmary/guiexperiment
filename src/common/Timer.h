#pragma once
#include <functional>

class Timer {
    bool clear = false;

public:
    
    //void set_timeout(void (*function)(), int delay);
    

    
    //void set_interval(void(*function)(), int interval);
    void set_interval(std::function<void()> func, int interval);

    void stop();
};

