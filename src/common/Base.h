#pragma once

#include <memory>

//using namespace std;

class Base : public std::enable_shared_from_this<Base>
{
    public:
        virtual ~Base()
        {}
};

template <class T>
class inheritable_shared_from_this : virtual public Base
{
    public:
        std::shared_ptr<T> shared_from_this() {
            return std::dynamic_pointer_cast<T>(Base::shared_from_this());
        }

        /* Utility method to easily downcast.
         * Useful when a child doesn't inherit directly from enable_shared_from_this
         * but wants to use the feature.
         */
        template <class Down>
        std::shared_ptr<Down> downcasted_shared_from_this() {
            return std::dynamic_pointer_cast<Down>(Base::shared_from_this());
        }
};