#include "Timer.h"
#include <thread>

/*
void Timer::set_timeout(auto function, int delay) {
    this->clear = false;
    std::thread t([=]() {
        if (this->clear) return;
        std::this_thread::sleep_for(std::chrono::milliseconds(delay));
        if (this->clear) return;
        function();
        });
    t.detach();
}
*/

void Timer::set_interval(std::function<void()> func, int interval) {
    this->clear = false;

    std::thread t([=]() {
        while (true) {
            if (this->clear) return;
            std::this_thread::sleep_for(std::chrono::milliseconds(interval));
            if (this->clear) return;
            func();
        }
    });

    t.detach();
}

void Timer::stop() {
	this->clear = true;
}