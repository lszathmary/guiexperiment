#pragma once
#include <SDL.h>
#include <memory>
#include "Common.h"
#include "Graphics.h"
#include "Screen.h"

using namespace std;


struct MouseState {
	int x, y;
	int mouse_down;
	bool active;

	void set_location(int x, int y) { this->x = x; this->y = y; }
};

class  Application
{
public:
	 Application();
	~Application();

	bool init();
	void run();
	void close();

	void set_screen(shared_ptr<Screen> screen);
	shared_ptr<Screen> get_screen();

	shared_ptr<Texture> load_texture(string path);

	shared_ptr<Texture> load_texture(string path, int borderSize);

	void move_mouse(int x, int y);
	void mouse_down(int x, int y);
	void mouse_up(int x, int y);
	void mouse_scroll(int delta);
	void on_resize();

	void assure_gamepad_input_mode();
	void assure_mouse_input_mode();

public:
	MouseState  mouse_state;
	SDL_Window* window;
	shared_ptr<Graphics> graphics;
	shared_ptr<Screen> screen;
	shared_ptr<Image> image;
	shared_ptr<Image> image2;
};



