#include "Graphics.h"

Graphics::Graphics(SDL_Window *window):
	mRenderer(nullptr) {

	TTF_Init();

	mRenderer = SDL_CreateRenderer(window, 1, SDL_RENDERER_ACCELERATED);
	
	//mFont.loadFromFile("data/fonts/consola.ttf", 16);
	mFont.loadFromFile("data/fonts/opensans-regular.ttf", 15);
	//mFont.loadFromFile("data/fonts/opensans-bold.ttf", 14);

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2");
	SDL_SetRenderDrawBlendMode(mRenderer, SDL_BLENDMODE_BLEND);
}

Graphics::~Graphics() {
	TTF_Quit();
	SDL_DestroyRenderer(mRenderer);
}

void Graphics::setFont(std::string name, int size) {
	mFont.loadFromFile(name, size);
}

void Graphics::clear() {
	SDL_RenderClear(mRenderer);
}

void Graphics::drawText(Rectangle &rectangle, std::string &text, Alignment aligment) {

	switch (aligment)
	{
		case Alignment::Left:
			drawTextLeft(rectangle, text);
			break;
		
		default:
			drawText(rectangle, text);
			break;
	}

}

void Graphics::drawText(Rectangle &rectangle, std::string &text) {
	int textWidth  = 0;
	int textHeight = 0;

	TTF_SizeText(mFont.ptr(), text.c_str(), &textWidth, &textHeight);
	   
	auto x = (textWidth > rectangle.width)
		? rectangle.x
		: rectangle.x + ((rectangle.width - textWidth) / 2);
	
	auto y = (textHeight > rectangle.height)
		? rectangle.y
		: rectangle.y + ((rectangle.height - textHeight) / 2);
	
	SDL_Rect rect = { x, y, rectangle.width, rectangle.height };

	// Should not use clipping here

	//save();

	//setClipRectangle(rectangle);
	drawText(x, y, text);

	//restore();
}

void Graphics::drawTextLeft(Rectangle &rectangle, std::string &text) {
	int textWidth  = 0;
	int textHeight = 0;

	TTF_SizeText(mFont.ptr(), text.c_str(), &textWidth, &textHeight);

	auto x = rectangle.x;
	auto y = (textHeight > rectangle.height)
		? rectangle.y
		: rectangle.y + ((rectangle.height - textHeight) / 2);
	
			
	drawText(x, y, text);
}

void Graphics::drawText(int x, int y, std::string &text) {
	int textureWidth = 0;
	int textureHeight = 0;

	SDL_Surface* surface = TTF_RenderText_Blended(mFont.ptr(), text.c_str(), renderColor());
	SDL_Texture* texture = SDL_CreateTextureFromSurface(mRenderer, surface);

	SDL_QueryTexture(texture, NULL, NULL, &textureWidth, &textureHeight);

	SDL_Rect source = { 0, 0, textureWidth, textureHeight };
	SDL_Rect destination = { x, y, textureWidth, textureHeight };

	SDL_RenderCopy(mRenderer, texture, &source, &destination);

	SDL_DestroyTexture(texture);
	SDL_FreeSurface(surface);
}

void Graphics::fillRectangle(Rectangle &rectangle) {
	SDL_RenderFillRect(mRenderer, (SDL_Rect*)(&rectangle));
}

void Graphics::drawRectangle(Rectangle &rectangle) {
	SDL_RenderDrawRect(mRenderer,(SDL_Rect*)(&rectangle));
}

Rectangle Graphics::clipRectangle(){
	SDL_Rect rectangle;

	SDL_RenderGetClipRect(mRenderer, &rectangle);

	return Rectangle(rectangle.x,rectangle.y, rectangle.w, rectangle.h);
}

void Graphics::setClipRectangle(Rectangle &rectangle) {
	SDL_RenderSetClipRect(mRenderer, (SDL_Rect*)(&rectangle));
}

bool Graphics::hasClipping() {
	return SDL_RenderIsClipEnabled(mRenderer);
}

void Graphics::clearClipRectangle() {
	SDL_RenderSetClipRect(mRenderer, NULL);
}

void Graphics::drawImage(Rectangle &rectangle, Image& image) {
	SDL_Texture* texture = SDL_CreateTextureFromSurface(mRenderer, image.ptr());

	SDL_RenderCopy(mRenderer, texture, NULL, (SDL_Rect*)(&rectangle));
	SDL_DestroyTexture(texture);
}

void Graphics::drawImage(int x, int y, Image& image) {
	SDL_Texture* texture = SDL_CreateTextureFromSurface(mRenderer, image.ptr());

	SDL_Rect rect = { x, y, 0, 0 };
	SDL_QueryTexture(texture, NULL, NULL, &rect.w, &rect.h);

	SDL_RenderCopy(mRenderer, texture, NULL, &rect);
	SDL_DestroyTexture(texture);
}

void Graphics::present() {
	SDL_RenderPresent(mRenderer);
}

Size Graphics::ouputSize(){
	int width, height;

	SDL_GetRendererOutputSize(mRenderer, &width, &height);

	return Size(width, height);
}

void Graphics::save() {
	SDL_Rect clip_rect;

	if (SDL_FALSE == SDL_RenderIsClipEnabled(mRenderer)) {
		clip_rect = { -1, -1, -1, -1 };
	}
	else {
		SDL_RenderGetClipRect(mRenderer, &clip_rect);
	}

	mClipStack.push(clip_rect);
}

void Graphics::restore() {
	if (mClipStack.size() == 0)
		return;
	
	SDL_Rect clip_rect = mClipStack.top();
	mClipStack.pop();
	
	if ((clip_rect.x == -1) && (clip_rect.y == -1)){
		SDL_RenderSetClipRect(mRenderer, NULL);
	}
	else {
		SDL_RenderSetClipRect(mRenderer, (SDL_Rect*)(&clip_rect));
	}
}

void Graphics::drawImage(Rectangle& dest, int border, Image& image) {
	SDL_Texture* texture = SDL_CreateTextureFromSurface(mRenderer, image.ptr());

	SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_ADD);

	// Draw the top Left
	SDL_Rect topLeftSrc  = { 0, 0, border, border };
	SDL_Rect topLeftDest = { dest.x,  dest.y, border, border };
	SDL_RenderCopy(mRenderer, texture, &topLeftSrc, &topLeftDest);

	// Draw the top right
	SDL_Rect topRightSrc  = { image.ptr()->w - border, 0, border, border };
	SDL_Rect topRightDest = { dest.x + dest.width - border,  dest.y, border, border };
	SDL_RenderCopy(mRenderer, texture, &topRightSrc, &topRightDest);

	// Draw the top center.
	SDL_Rect topSrc = {  border, 0, image.ptr()->w - 2 * border,  border };
	SDL_Rect topDest = { dest.x + border,  dest.y, dest.width - 2 * border,  border };
	SDL_RenderCopy(mRenderer, texture, &topSrc, &topDest);

	// Draw the left middle
	SDL_Rect leftSrc = { 0, border, border, image.ptr()->h - 2 * border };
	SDL_Rect leftDest = { dest.x,  dest.y + border, border, dest.height - 2 * border };
	SDL_RenderCopy(mRenderer, texture, &leftSrc, &leftDest);

	// Draw the right center
	SDL_Rect rightSrc = { (image.ptr()->w) - border, border, border,  image.ptr()->h - 2 * border };
	SDL_Rect rightDest = { dest.x + dest.width - border,  dest.y + border, border, dest.height - 2 * border };
	SDL_RenderCopy(mRenderer, texture, &rightSrc, &rightDest);
	
	// Draw the bottom left.
	SDL_Rect bottomLeftSrc = { 0, image.ptr()->h - border, border, border };
	SDL_Rect bottomLeftDest = { dest.x,  dest.y + dest.height - border, border, border };
	SDL_RenderCopy(mRenderer, texture, &bottomLeftSrc, &bottomLeftDest);

	// Draw the bottom right.
	SDL_Rect bottomRightSrc = { image.ptr()->w - border, image.ptr()->h - border, border, border };
	SDL_Rect bottomRightDest = { dest.x + dest.width - border,  dest.y + dest.height - border, border, border };
	SDL_RenderCopy(mRenderer, texture, &bottomRightSrc, &bottomRightDest);

	// Draw the bottom center.
	SDL_Rect bottomSrc = { border, image.ptr()->h - border, image.ptr()->w - 2 * border, border };
	SDL_Rect bottomDest = { dest.x + border,  dest.y + dest.height - border, dest.width - 2 * border, border };
	SDL_RenderCopy(mRenderer, texture, &bottomSrc, &bottomDest);

	// Draw center 
	SDL_Rect centerSrc = { border, border, image.ptr()->w - 2 * border, image.ptr()->h - 2 *border };
	SDL_Rect centerDest = { dest.x + border,  dest.y + border, dest.width - 2 * border,dest.height -2* border };
	
	SDL_RenderCopy(mRenderer, texture, &centerSrc, &centerDest);
	SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);
	SDL_DestroyTexture(texture);
}

void Graphics::drawTexture(int x, int y, Texture &texture) {
	if (texture.borderSize() != 0){
		drawTextureInternal(x, y, texture.borderSize(), texture);
	}
	else {
		drawTextureInternal(x, y, texture);
	}
}

void Graphics::drawTexture(Rectangle& rectangle, Texture& texture) {
	if (texture.borderSize() != 0){
		drawTextureInternal(rectangle, texture.borderSize(), texture);
	}
	else {
		drawTextureInternal(rectangle, texture);
	}
}

void Graphics::setBlendMode(SDL_BlendMode mode){
	SDL_SetRenderDrawBlendMode(mRenderer, mode);
}

SDL_BlendMode Graphics::blendMode(){
	SDL_BlendMode mode;
	SDL_GetRenderDrawBlendMode(mRenderer, &mode);

	return mode;
}

void Graphics::setRenderColor(Color &color){
	SDL_SetRenderDrawColor(mRenderer, color.red, color.green, color.blue, color.alpha);
}

Color Graphics::renderColor(){
	Color color;

	SDL_GetRenderDrawColor(mRenderer, &color.red, &color.green, &color.blue, &color.alpha);

	return color;
}

void Graphics::drawTextureInternal(Rectangle& dest, int borderSize, Texture& texture) {
	// https://en.wikipedia.org/wiki/9-slice_scaling
	
	SDL_SetTextureBlendMode(texture.ptr(), SDL_BLENDMODE_BLEND);
	int textureWidth = 0;
	int textureHeight = 0;

	SDL_QueryTexture(texture.ptr(), NULL, NULL, &textureWidth, &textureHeight);

	// Draw the top Left
	SDL_Rect topLeftSrc = { 0, 0, borderSize, borderSize };
	SDL_Rect topLeftDest = { dest.x,  dest.y, borderSize, borderSize };
	SDL_RenderCopy(mRenderer, texture.ptr(), &topLeftSrc, &topLeftDest);

	// Draw the top right
	SDL_Rect topRightSrc = { textureWidth - borderSize, 0, borderSize, borderSize };
	SDL_Rect topRightDest = { dest.x + dest.width - borderSize,  dest.y, borderSize, borderSize };
	SDL_RenderCopy(mRenderer, texture.ptr(), &topRightSrc, &topRightDest);

	// Draw the top center
	SDL_Rect topSrc = { borderSize, 0, textureWidth - 2 * borderSize,  borderSize };
	SDL_Rect topDest = { dest.x + borderSize,  dest.y, dest.width - 2 * borderSize,  borderSize };
	SDL_RenderCopy(mRenderer, texture.ptr(), &topSrc, &topDest);

	// Draw the left middle
	SDL_Rect leftSrc = { 0, borderSize, borderSize, textureHeight - 2 * borderSize };
	SDL_Rect leftDest = { dest.x,  dest.y + borderSize, borderSize, dest.height - 2 * borderSize };
	SDL_RenderCopy(mRenderer, texture.ptr(), &leftSrc, &leftDest);

	// Draw the right center
	SDL_Rect rightSrc = { (textureWidth) - borderSize, borderSize, borderSize,  textureHeight - 2 * borderSize };
	SDL_Rect rightDest = { dest.x + dest.width - borderSize,  dest.y + borderSize, borderSize, dest.height - 2 * borderSize };
	SDL_RenderCopy(mRenderer, texture.ptr(), &rightSrc, &rightDest);

	// Draw the bottom left
	SDL_Rect bottomLeftSrc = { 0, textureHeight - borderSize, borderSize, borderSize };
	SDL_Rect bottomLeftDest = { dest.x,  dest.y + dest.height - borderSize, borderSize, borderSize };
	SDL_RenderCopy(mRenderer, texture.ptr(), &bottomLeftSrc, &bottomLeftDest);

	// Draw the bottom right.
	SDL_Rect bottomRightSrc = { textureWidth - borderSize, textureHeight - borderSize, borderSize, borderSize };
	SDL_Rect bottomRightDest = { dest.x + dest.width - borderSize,  dest.y + dest.height - borderSize, borderSize, borderSize };
	SDL_RenderCopy(mRenderer, texture.ptr(), &bottomRightSrc, &bottomRightDest);

	// Draw the bottom center.
	SDL_Rect bottomSrc = { borderSize,textureHeight - borderSize,textureWidth - 2 * borderSize, borderSize };
	SDL_Rect bottomDest = { dest.x + borderSize,  dest.y + dest.height - borderSize, dest.width - 2 * borderSize, borderSize };
	SDL_RenderCopy(mRenderer, texture.ptr(), &bottomSrc, &bottomDest);

	// Draw center 
	SDL_Rect centerSrc = { borderSize, borderSize, textureWidth - 2 * borderSize, textureHeight - 2 * borderSize };
	SDL_Rect centerDest = { dest.x + borderSize,  dest.y + borderSize, dest.width - 2 * borderSize,dest.height - 2 * borderSize };

	SDL_RenderCopy(mRenderer, texture.ptr(), &centerSrc, &centerDest);
	SDL_SetTextureBlendMode(texture.ptr(), SDL_BLENDMODE_BLEND);
}

void Graphics::drawTextureInternal(Rectangle& rectangle, Texture& texture) {
	SDL_RenderCopy(mRenderer, texture.ptr(), NULL, (SDL_Rect*)(&rectangle));
}

void Graphics::drawTextureInternal(int x, int y, Texture &texture) {
	auto destination = Rectangle(x, y, texture.size().width, texture.size().height);

	drawTextureInternal(destination, texture);
}

void Graphics::drawTextureInternal(int x, int y, int borderSize, Texture &texture) {
	auto destination = Rectangle(x, y, texture.size().width, texture.size().height);

	drawTextureInternal(destination, borderSize, texture);
}