#pragma once

#include <string>
#include <memory>
#include <SDL.h>
#include "Common.h"

class Graphics;

/**
 *
 */
class Texture
{
public:
	Texture(std::shared_ptr<Graphics> graphics);
	~Texture();

	// Return a pointer to texture
	SDL_Texture* ptr() { return mTexture;  }

	// Return the border size, used onyl for border textures
	int borderSize() { return mBorderSize; }

	// Return the size of the texture.
	Size size();

	// Load the texture from the specified path
	void loadFromFile(std::string path);
	
	// Load a border texture from the specified path
	void loadFromFile(std::string path, int borderSize);

private:
	int mBorderSize;
	SDL_Texture* mTexture;
	SDL_Renderer* mRenderer;
};

