#include "Image.h"
#include <SDL_image.h>

Image::Image()
    :mSurface(NULL) {

}

Image::~Image() {
	SDL_FreeSurface(mSurface);
}

void Image::loadFromFile(std::string path) {
    if (mSurface != NULL) {
        SDL_FreeSurface(mSurface);
    }

    mSurface = IMG_Load(path.c_str());
}

void Image::loadFromFile(std::string path, int width, int height) {
    if (this->mSurface != NULL) {
        SDL_FreeSurface(mSurface);
    }
    // The image quaility is bad
    SDL_Surface* buffer = IMG_Load(path.c_str());
    SDL_Rect rectangle = { 0, 0, width, height };

    this->mSurface = SDL_CreateRGBSurface(
        buffer->flags,
        width,
        height,
        32,
        buffer->format->Rmask,
        buffer->format->Gmask,
        buffer->format->Bmask,
        buffer->format->Amask);

    SDL_BlitScaled(buffer, NULL, mSurface, &rectangle);
    SDL_FreeSurface(buffer);
}

void Image::loadFromFile(std::string path, int width, int height, int border) {
    SDL_Surface* buffer = IMG_Load(path.c_str());
    
    this->mSurface = SDL_CreateRGBSurface(
        buffer->flags, width, height, 32,
        buffer->format->Rmask,
        buffer->format->Gmask,
        buffer->format->Bmask,
        buffer->format->Amask);

    SDL_Rect top_left_src  = { 0, 0, border, border };
    SDL_Rect top_left_dest = { 0, 0, border, border };

    SDL_Rect top_right_src = { buffer->w - border, 0, border, border };
    SDL_Rect top_right_dest = { width - border, 0, border, border };
    
    SDL_BlitScaled(buffer, &top_left_src, mSurface, &top_left_dest);
    SDL_BlitScaled(buffer, &top_right_src, mSurface, &top_right_dest);

    SDL_Rect top_src = { border, 0, buffer->w - 2 *border,  border };
    SDL_Rect top_dest = { border, 0, width - 2 * border, border};
        
    SDL_BlitScaled(buffer, &top_src, mSurface, &top_dest);

    SDL_Rect left_src = { 0, border, border, buffer->h - 2 * border };
    SDL_Rect left_dest = { 0, border, border, height - 2 * border };

    SDL_Rect right_src = { (buffer->w) - border, border, border,  buffer->h - 2* border};
    SDL_Rect right_dest = { width - border, border, border, height -2 * border};
  
    SDL_BlitScaled(buffer, &left_src, mSurface, &left_dest);
    SDL_BlitScaled(buffer, &right_src, mSurface, &right_dest);

    SDL_Rect bottom_left_src = {0, buffer->h - border, border, border};
    SDL_Rect bottom_left_dest = { 0, height - border, border, border };

    SDL_Rect bottom_right_src = { buffer->w - border, buffer->h - border, border, border };
    SDL_Rect bottom_right_dest = { width - border, height - border, border, border };

    SDL_BlitScaled(buffer, &bottom_left_src, mSurface, &bottom_left_dest);
    SDL_BlitScaled(buffer, &bottom_right_src, mSurface, &bottom_right_dest);

    SDL_Rect bottom_src = { border, buffer->h - border, buffer->w - 2*border, border};
    SDL_Rect bottom_dest = { border, height-border, width - 2*border, border };

    SDL_BlitScaled(buffer, &bottom_src, mSurface, &bottom_dest);
    SDL_FreeSurface(buffer);
}

