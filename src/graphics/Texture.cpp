#include "Texture.h"
#include "Graphics.h"
#include <SDL_image.h>


Texture::Texture(std::shared_ptr<Graphics> graphics):mTexture(NULL){
    mRenderer = graphics->ptr();
}

Texture::~Texture() {
    SDL_DestroyTexture(mTexture);
}

Size Texture::size(){
    Size size;
    
    SDL_QueryTexture(mTexture, NULL, NULL, &size.width, &size.height);

    return size;
}

void Texture::loadFromFile(std::string path){	
    if (mTexture != NULL) {
        SDL_DestroyTexture(mTexture);
        mTexture = NULL;
    }
    
    SDL_Surface* surface = IMG_Load(path.c_str());
	mTexture = SDL_CreateTextureFromSurface(mRenderer, surface);
    mBorderSize = 0;

    SDL_FreeSurface(surface);
}

void Texture::loadFromFile(std::string path, int borderSize){
    if (mTexture != NULL) {
        SDL_DestroyTexture(mTexture);
        mTexture = NULL;
    }

    SDL_Surface* surface = IMG_Load(path.c_str());
	mTexture = SDL_CreateTextureFromSurface(mRenderer, surface);
    mBorderSize = borderSize;

    SDL_FreeSurface(surface);
}
