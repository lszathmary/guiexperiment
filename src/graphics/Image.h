#pragma once

#include <SDL.h>
#include <string>

class Image
{
public:
	Image();
	~Image();

	SDL_Surface *ptr() { return mSurface; }

	void loadFromFile(std::string path);
	void loadFromFile(std::string path, int width, int height);
	void loadFromFile(std::string path, int width, int height, int border);

private:
	SDL_Surface* mSurface;
};

