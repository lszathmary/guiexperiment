#include "Font.h"


Font::Font():mFont(NULL){

}

Font::~Font(){
    TTF_CloseFont(mFont);
}

void Font::loadFromFile(std::string path, int size){
    if (mFont != NULL){
        TTF_CloseFont(mFont);
    }

    mFont = TTF_OpenFont(path.c_str(), size);
}