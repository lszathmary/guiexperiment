#pragma once

#include <string>
#include <SDL_ttf.h>

class Font
{
public:
	Font();
	~Font();

	TTF_Font* ptr() { return mFont; }

	void loadFromFile(std::string path, int size);
private:
	TTF_Font* mFont;
};

