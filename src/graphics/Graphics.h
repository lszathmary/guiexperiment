#pragma once

#include <SDL_ttf.h>
#include <SDL_image.h>
#include <string>
#include <vector>
#include <stack>
#include <iostream>

#include "Common.h"
#include "Image.h"
#include "Texture.h"
#include "Font.h"

class Graphics
{
public:
	Graphics(SDL_Window *window);
	~Graphics();
	
	SDL_Renderer* ptr() { return mRenderer; }

	SDL_BlendMode blendMode();
	void setBlendMode(SDL_BlendMode mode);

	Color renderColor();
	void setRenderColor(Color &color);

	Font font() { return mFont;  }
	void setFont(Font font) { mFont = font; }
	void setFont(std::string path, int size);

	Rectangle clipRectangle();
	void setClipRectangle(Rectangle &rectangle);

	bool hasClipping();
	void clearClipRectangle();

	void save();
	void restore();

	void drawText(Rectangle &rectangle, std::string &text, Alignment aligment);
	void drawText(Rectangle &rectangle, std::string &text);
	void drawText(int x, int y, std::string &text);

	void fillRectangle(Rectangle &rectangle);
	void drawRectangle(Rectangle &rectangle);

	void drawImage(Rectangle &rectangle, Image& image);
	void drawImage(int x, int y, Image& image);
	void drawImage(Rectangle &rectangle, int border,Image &image);

	void drawTexture(int x, int y, Texture &texture);
	void drawTexture(Rectangle& dest, Texture& texture);
			
	void clear();
	void present();

	Size ouputSize();
private:

	// Draw a normal texture to the specified location
	void drawTextureInternal(int x, int y, Texture &texture);

	// Draw a border texture to the specified location ???
	void drawTextureInternal(int x, int y, int borderSize, Texture &texture);

	// Draw a normal texture to the specified destination
	void drawTextureInternal(Rectangle& dest, Texture& texture);

	// Draw a border texture to the specified destination
	void drawTextureInternal(Rectangle &dest, int borderSize, Texture &texture);

	void drawTextLeft(Rectangle &rectangle, std::string &text);

private:
	// Font used for drawing text
	Font mFont;
	// Contains rendering state
	SDL_Renderer* mRenderer;
	// The clip rectangle stack 
	std::stack<SDL_Rect> mClipStack;

	friend class Texture;
};

